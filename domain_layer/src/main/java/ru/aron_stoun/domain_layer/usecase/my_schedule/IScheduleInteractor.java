package ru.aron_stoun.domain_layer.usecase.my_schedule;

import java.util.List;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.models.MyScheduleWeeks;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.models.Weekday;


public interface IScheduleInteractor {

    Observable<List<Weekday>> getMySchedule(String date, int id, int targetType);
    Observable<MyScheduleWeeks> getMyScheduleWeeks();
    Observable<List<PairTime>> getPairTimeSchedule();
}
