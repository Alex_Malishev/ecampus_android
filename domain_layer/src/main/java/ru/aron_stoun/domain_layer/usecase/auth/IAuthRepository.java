package ru.aron_stoun.domain_layer.usecase.auth;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.models.Credentials;

public interface IAuthRepository {

    Observable<Boolean> login(String requestToken, String login, String password, boolean rememberMe);

    Observable<String> getRequestToken();


    Observable<Credentials> checkUserCredentialsExist();

}
