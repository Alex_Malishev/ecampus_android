package ru.aron_stoun.domain_layer.usecase.auth;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import ru.aron_stoun.domain_layer.models.Credentials;

public class AuthInteractor implements IAuthInteractor {

    private IAuthRepository authRepository;

    public AuthInteractor(IAuthRepository authRepository) {
        this.authRepository = authRepository;
    }


    @Override
    public Observable<Boolean> login(String login, String password, boolean remeberMe) {
        Observable<String> requestTokenObservable = authRepository.getRequestToken();
        return requestTokenObservable
                .concatMap(s -> {
                    if (s != null) {
                        return authRepository.login(s, login, password, remeberMe);
                    }else {
                        return Observable.just(false);
                    }
                });
    }

    @Override
    public Observable<Boolean> tryToLoginIfCredentialExist() {
        Observable<Credentials> credentialsObservable = authRepository.checkUserCredentialsExist();
        return credentialsObservable != null ?
                credentialsObservable.concatMap((Function<Credentials, ObservableSource<Boolean>>)
                        credentials -> login(credentials.getLogin(), credentials.getPassword(), false))
                : Observable.just(false);
    }

}
