package ru.aron_stoun.domain_layer.usecase.my_schedule;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.models.MyScheduleWeeks;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.models.Weekday;


public interface IScheduleRepository {

    Observable<List<Weekday>> getMySchedule(Date date, int id, int targerType);
    Observable<MyScheduleWeeks> getMyScheduleWeeks();
    Observable<List<PairTime>> getPairTimeSchedule();
}
