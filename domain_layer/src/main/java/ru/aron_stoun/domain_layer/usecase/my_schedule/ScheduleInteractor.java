package ru.aron_stoun.domain_layer.usecase.my_schedule;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.DateUtils;
import ru.aron_stoun.domain_layer.models.MyScheduleWeeks;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.models.Weekday;


public class ScheduleInteractor implements IScheduleInteractor {


    private IScheduleRepository scheduleRepository;

    public ScheduleInteractor(IScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }


    @Override
    public Observable<List<Weekday>> getMySchedule(String date, int id, int targetType) {
        Date dateStart = DateUtils.dateFromString(date);
        return scheduleRepository.getMySchedule(dateStart, id, targetType);
    }

    @Override
    public Observable<MyScheduleWeeks> getMyScheduleWeeks() {
        return scheduleRepository.getMyScheduleWeeks();
    }

    @Override
    public Observable<List<PairTime>> getPairTimeSchedule() {
        return scheduleRepository.getPairTimeSchedule();
    }
}
