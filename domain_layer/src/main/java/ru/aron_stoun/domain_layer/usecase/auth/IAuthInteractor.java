package ru.aron_stoun.domain_layer.usecase.auth;

import io.reactivex.Observable;

public interface IAuthInteractor {

    Observable<Boolean> login(String login, String password, boolean remeberMe);

    Observable<Boolean> tryToLoginIfCredentialExist();
}
