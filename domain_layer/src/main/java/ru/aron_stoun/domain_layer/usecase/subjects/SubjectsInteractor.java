package ru.aron_stoun.domain_layer.usecase.subjects;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Lesson;
import ru.aron_stoun.domain_layer.models.Speciality;


public class SubjectsInteractor implements ISubjectInteractor {


    private ISubjectsRepository subjectsRepository;

    public SubjectsInteractor(ISubjectsRepository subjectsRepository) {
        this.subjectsRepository = subjectsRepository;
    }

    @Override
    public Observable<List<Speciality>> getSpecialities() {
        return subjectsRepository.getSpecialities();
    }

    @Override
    public Observable<List<Course>> getCourses(int specialityId, int termId) {
        return subjectsRepository.getCourses(specialityId, termId);
    }

    @Override
    public Observable<List<Lesson>> getLessons(int specialityId, int lessonTypeId) {
        return subjectsRepository.getLessons(specialityId, lessonTypeId);
    }
}
