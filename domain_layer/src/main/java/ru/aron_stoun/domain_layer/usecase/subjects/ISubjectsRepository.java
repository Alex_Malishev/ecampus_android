package ru.aron_stoun.domain_layer.usecase.subjects;

import java.util.List;

import io.reactivex.Observable;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Lesson;
import ru.aron_stoun.domain_layer.models.Speciality;


public interface ISubjectsRepository {


    Observable<List<Speciality>> getSpecialities();
    Observable<List<Course>> getCourses(int specialityId, int termId);
    Observable<List<Lesson>> getLessons(int specialityId, int lessonTypeId);
}
