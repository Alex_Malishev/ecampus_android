package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LessonType {

    @SerializedName("Id")
    private int id;
    @SerializedName("ParentId")
    private int parentId;
    @SerializedName("Name")
    private String name;
    @SerializedName("SchoolType")
    private boolean schoolType;
    @SerializedName("Lessons")
    private ArrayList<Lesson> lessons;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSchoolType() {
        return schoolType;
    }

    public void setSchoolType(boolean schoolType) {
        this.schoolType = schoolType;
    }

    public ArrayList<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(ArrayList<Lesson> lessons) {
        this.lessons = lessons;
    }

    public double calculateRating(){
        if (lessons == null || lessons.isEmpty()){
            return 0d;
        }

        double rating = 0d;
        for (Lesson lesson : lessons) {
            rating += lesson.getGainedScore();
        }
        return rating;
    }
}
