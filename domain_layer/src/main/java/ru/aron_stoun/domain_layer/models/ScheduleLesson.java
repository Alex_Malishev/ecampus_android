package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ScheduleLesson {

    @SerializedName("Id")
    private int id;
    @SerializedName("LessonDayId")
    private int lessonDayId;
    @SerializedName("TeacherId")
    private int teacherId;
    @SerializedName("RoomId")
    private int roomId;
    @SerializedName("Discipline")
    private String discipline;
    @SerializedName("TimeBegin")
    private String timeBegin;
    @SerializedName("TimeEnd")
    private String timeEnd;
    @SerializedName("Aud")
    private Classroom classroom;
    @SerializedName("LessonType")
    private String lessonType;
    @SerializedName("PairNumberStart")
    private int pairNumberStart;
    @SerializedName("PairNumberEnd")
    private int pairNumberEnd;
    @SerializedName("Teacher")
    private Teacher teacher;
    @SerializedName("Groups")
    private ArrayList<Group> groups;
    @SerializedName("Current")
    private boolean current;
    @SerializedName("LessonName")
    private String lessonName;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLessonDayId() {
        return lessonDayId;
    }

    public void setLessonDayId(int lessonDayId) {
        this.lessonDayId = lessonDayId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }


    public String getLessonType() {
        return lessonType;
    }

    public void setLessonType(String lessonType) {
        this.lessonType = lessonType;
    }

    public int getPairNumberStart() {
        return pairNumberStart;
    }

    public void setPairNumberStart(int pairNumberStart) {
        this.pairNumberStart = pairNumberStart;
    }

    public int getPairNumberEnd() {
        return pairNumberEnd;
    }

    public void setPairNumberEnd(int pairNumberEnd) {
        this.pairNumberEnd = pairNumberEnd;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "ScheduleLesson{" +
                "id=" + id +
                ", lessonDayId=" + lessonDayId +
                ", teacherId=" + teacherId +
                ", roomId=" + roomId +
                ", discipline='" + discipline + '\'' +
                ", timeBegin='" + timeBegin + '\'' +
                ", timeEnd='" + timeEnd + '\'' +
                ", classroom=" + classroom +
                ", lessonType='" + lessonType + '\'' +
                ", pairNumberStart=" + pairNumberStart +
                ", pairNumberEnd=" + pairNumberEnd +
                ", teacher=" + teacher +
                ", groups=" + groups +
                ", current=" + current +
                ", lessonName='" + lessonName + '\'' +
                '}';
    }
}
