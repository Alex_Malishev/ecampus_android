package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

public class Teacher {

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
