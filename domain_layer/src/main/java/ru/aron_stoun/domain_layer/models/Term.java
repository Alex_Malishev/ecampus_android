package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Term {


    @SerializedName("Id")
    private int id;
    @SerializedName("ParentId")
    private int parentId;
    @SerializedName("Name")
    private String name;
    @SerializedName("IsCurrent")
    private boolean isCurrent;
    @SerializedName("TermTypeName")
    private String termTypeName;
    @SerializedName("Courses")
    private ArrayList<Course> courses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public String getTermTypeName() {
        return termTypeName;
    }

    public void setTermTypeName(String termTypeName) {
        this.termTypeName = termTypeName;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
}
