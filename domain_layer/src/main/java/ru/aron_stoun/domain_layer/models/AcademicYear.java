package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AcademicYear {

    @SerializedName("Id")
    private int id;
    @SerializedName("ParentId")
    private int parentId;
    @SerializedName("Name")
    private String name;
    @SerializedName("KursTypeName")
    private String courseTypeName;
    @SerializedName("Terms")
    private ArrayList<Term> terms;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }

    public ArrayList<Term> getTerms() {
        return terms;
    }

    public void setTerms(ArrayList<Term> terms) {
        this.terms = terms;
    }

    public boolean isCurrent(){
        if (terms == null) return false;
        for (Term term : terms) {
            if (term.isCurrent()) return true;
        }
        return false;
    }
}
