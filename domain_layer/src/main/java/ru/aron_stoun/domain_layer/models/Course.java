package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Course {

    @SerializedName("Id")
    private int id;
    @SerializedName("ParentId")
    private int parentId;
    @SerializedName("Name")
    private String name;
    @SerializedName("MaxRating")
    private int maxRating;
    @SerializedName("CurrentRating")
    private double currentRating;
    @SerializedName("LessonTypes")
    private ArrayList<LessonType> lessonTypes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRating() {
        return maxRating;
    }

    public void setMaxRating(int maxRating) {
        this.maxRating = maxRating;
    }

    public double getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(double currentRating) {
        this.currentRating = currentRating;
    }

    public ArrayList<LessonType> getLessonTypes() {
        return lessonTypes;
    }

    public void setLessonTypes(ArrayList<LessonType> lessonTypes) {
        this.lessonTypes = lessonTypes;
    }
}
