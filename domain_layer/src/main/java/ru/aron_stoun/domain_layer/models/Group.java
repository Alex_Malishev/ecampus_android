package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

public class Group {

    @SerializedName("Id")
    private int id;
    @SerializedName("LessonId")
    private int lessonId;
    @SerializedName("Name")
    private String name;
    @SerializedName("Subgroup")
    private String subgroup;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }
}
