package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Weekday {

    @SerializedName("WeekDay")
    private String name;
    @SerializedName("Date")
    private String date;
    @SerializedName("Lessons")
    private ArrayList<ScheduleLesson> lessons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<ScheduleLesson> getLessons() {
        return lessons;
    }

    public void setLessons(ArrayList<ScheduleLesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Weekday{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", lessons=" + printLessons() +
                '}';
    }

    private String printLessons(){
        StringBuilder result = new StringBuilder();
        for (ScheduleLesson lesson : lessons) {
            result.append(lesson.toString()).append("\n");
        }
        return result.toString();
    }
}
