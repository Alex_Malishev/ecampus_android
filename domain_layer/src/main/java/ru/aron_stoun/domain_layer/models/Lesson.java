package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

public class Lesson {

    @SerializedName("Id")
    private int id;
    @SerializedName("ParentId")
    private int parentId;
    @SerializedName("Attendance")
    private int attendance;
    @SerializedName("Date")
    private String date;
    @SerializedName("GainedScore")
    private double gainedScore;
    @SerializedName("GradeText")
    private String gradeText;
    @SerializedName("IsCheckpoint")
    private boolean isCheckpoint;
    @SerializedName("LostScore")
    private double lostScore;
    @SerializedName("PenaltyScore")
    private double penaltyScore;
    @SerializedName("Room")
    private String room;
    @SerializedName("ShortName")
    private String shortName;
    @SerializedName("Teacher")
    private String teacher;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getGainedScore() {
        return gainedScore;
    }

    public void setGainedScore(double gainedScore) {
        this.gainedScore = gainedScore;
    }

    public String getGradeText() {
        return gradeText;
    }

    public void setGradeText(String gradeText) {
        this.gradeText = gradeText;
    }

    public boolean isCheckpoint() {
        return isCheckpoint;
    }

    public void setCheckpoint(boolean checkpoint) {
        isCheckpoint = checkpoint;
    }

    public double getLostScore() {
        return lostScore;
    }

    public void setLostScore(double lostScore) {
        this.lostScore = lostScore;
    }

    public double getPenaltyScore() {
        return penaltyScore;
    }

    public void setPenaltyScore(double penaltyScore) {
        this.penaltyScore = penaltyScore;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
