package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Speciality {

    @SerializedName("Id")
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("AcademicYears")
    private ArrayList<AcademicYear> academicYears;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<AcademicYear> getAcademicYears() {
        return academicYears;
    }

    public void setAcademicYears(ArrayList<AcademicYear> academicYears) {
        this.academicYears = academicYears;
    }
}
