package ru.aron_stoun.domain_layer.models;

import com.google.gson.annotations.SerializedName;

public class Week {

    public static final int FIRST = 1;
    public static final int SECOND = 2;

    @SerializedName("DateBegin")
    private String dateBegin;
    @SerializedName("DateEnd")
    private String dateEnd;
    @SerializedName("Number")
    private int number;


    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
