package ru.aron_stoun.data_layer;

import android.annotation.SuppressLint;
import android.content.Context;

import ru.aron_stoun.data_layer.injection.DaggerDataLayerComponent;
import ru.aron_stoun.data_layer.injection.DataLayerComponent;
import ru.aron_stoun.data_layer.injection.DataLayerModule;
import ru.aron_stoun.data_layer.injection.DataSourceModule;
import ru.aron_stoun.data_layer.injection.DatabaseModule;
import ru.aron_stoun.data_layer.injection.NetModule;

@SuppressLint("StaticFieldLeak")
public class DataLayer {

    private static DataLayer sInstance;

    private static Context context;

    public static DataLayerComponent init(Context context){
        if (sInstance == null){
            sInstance = new DataLayer(context);
        }

        return createDataLayerComponent(context);
    }

    private DataLayer(Context context) {
        DataLayer.context = context;

    }

    private static DataLayerComponent createDataLayerComponent(Context context){
        return DaggerDataLayerComponent.builder()
                .dataLayerModule(new DataLayerModule(context))
                .dataSourceModule(new DataSourceModule())
                .netModule(new NetModule())
                .databaseModule(new DatabaseModule(context))
                .build();
    }

    public static Context get() {
        return context;
    }
}
