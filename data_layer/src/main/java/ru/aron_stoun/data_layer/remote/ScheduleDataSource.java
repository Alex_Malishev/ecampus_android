package ru.aron_stoun.data_layer.remote;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.aron_stoun.data_layer.model.requests.ScheduleRequest;
import ru.aron_stoun.data_layer.model.responses.PairTimeScheduleResponse;
import ru.aron_stoun.domain_layer.models.Weekday;


public interface ScheduleDataSource {


    @GET("schedule/my/student")
    Observable<ResponseBody> getMyStudy();

    @Headers("Content-Type: application/json")
    @POST("Schedule/GetSchedule")
    Observable<List<Weekday>> getSchedule(@Body ScheduleRequest scheduleRequest);

    @POST("schedule/GetPairsTimeSchedule")
    Observable<PairTimeScheduleResponse> getPairTimeSchedule();
}
