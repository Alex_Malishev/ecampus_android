package ru.aron_stoun.data_layer.remote;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.aron_stoun.data_layer.model.requests.CoursesRequest;
import ru.aron_stoun.data_layer.model.requests.LessonsRequest;
import ru.aron_stoun.data_layer.model.responses.CoursesResponse;
import ru.aron_stoun.domain_layer.models.Lesson;
import ru.aron_stoun.domain_layer.models.Speciality;


public interface SubjectsDataSource {


    @POST("studies/StudentGetSpecialties")
    Observable<List<Speciality>> getSpecialities();

    @Headers("Content-Type: application/json")
    @POST("studies/GetCourses")
    Observable<CoursesResponse> getCourses(@Body CoursesRequest coursesRequest);

    @Headers("Content-Type: application/json")
    @POST("studies/GetLessons")
    Observable<List<Lesson>> getLessons(@Body LessonsRequest lessonsRequest);


}
