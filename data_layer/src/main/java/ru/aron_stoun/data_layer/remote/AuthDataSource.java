package ru.aron_stoun.data_layer.remote;


import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthDataSource {

    @GET("account/login")
    Observable<Response<ResponseBody>> getRequestVerificationToken();

    @FormUrlEncoded
    @POST("account/login")
    Observable<Response<ResponseBody>> login(@Field("__RequestVerificationToken") String requestToke,
                                             @Field("Login") String login,
                                             @Field("Password") String password,
                                             @Field("RememberMe") Boolean rememberMe);
}
