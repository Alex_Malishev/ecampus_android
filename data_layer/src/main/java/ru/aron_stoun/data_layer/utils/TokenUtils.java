package ru.aron_stoun.data_layer.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by jarvi on 15.08.2017.
 */

public class TokenUtils {

    private static final String TOKEN_STORAGE = "token_storage";
    private static final String TOKEN = "token";
    private static String userPhone;


    public static String getToken(Context context){
        SharedPreferences sPref = context.getSharedPreferences(TOKEN_STORAGE, Application.MODE_PRIVATE);
        return sPref.getString(TOKEN, "");
    }

    public static void clearToken(Context context){
        SharedPreferences sPref = context.getSharedPreferences(TOKEN_STORAGE, Application.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.clear();
        ed.apply();
    }

    public static void saveToken(Context context, String token){
        SharedPreferences sPref = context.getSharedPreferences(TOKEN_STORAGE, Application.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.clear();
        ed.putString(TOKEN, token);
        ed.apply();
    }
}
