package ru.aron_stoun.data_layer.utils;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by jarvi on 02.07.2017.
 */
@Singleton
public class ServiceGenerator {

    private OkHttpClient.Builder httpClient;
    private Retrofit.Builder builder;
    private Retrofit retrofit;
    private AuthenticationInterceptor authenticationInterceptor;
    private TokenStatusInterceptor tokenStatusInterceptor;
    private ConnectionInterceptor connectionInterceptor;
    private JavaNetCookieJar javaNetCookieJar;
    private CookieManager cookieManager;


    @Inject
    public ServiceGenerator(OkHttpClient.Builder httpClient,
                            Retrofit.Builder retrofitBuilder,
                            AuthenticationInterceptor authenticationInterceptor,
                            TokenStatusInterceptor tokenStatusInterceptor,
                            ConnectionInterceptor connectionInterceptor) {
        this.httpClient = httpClient;
        this.builder = retrofitBuilder;
        this.authenticationInterceptor = authenticationInterceptor;
        this.tokenStatusInterceptor = tokenStatusInterceptor;
        this.connectionInterceptor = connectionInterceptor;
        addInterceptors();
    }

    private void addInterceptors(){
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        this.javaNetCookieJar = new JavaNetCookieJar(cookieManager);
        this.httpClient.authenticator(new TokenAuthenticator());
        this.httpClient.addInterceptor(this.authenticationInterceptor);
        this.httpClient.addInterceptor(this.tokenStatusInterceptor);
        this.httpClient.addInterceptor(this.connectionInterceptor);
    }


    public List<HttpCookie> getCookies() {
        return cookieManager.getCookieStore().getCookies();
    }

    public <S> S createService(Class<S> serviceClass) {


        builder.client(httpClient
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .cookieJar(javaNetCookieJar)
                .build());


        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }
}
