package ru.aron_stoun.data_layer.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Created by aron_stoun on 16.10.17.
 */

public class TokenStatusInterceptor implements Interceptor {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private static final String TAG = TokenStatusInterceptor.class.getSimpleName();

    @Inject
    public TokenStatusInterceptor() {
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        ResponseBody responseBody = response.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // Buffer the entire body.
        Buffer buffer = source.buffer();
        String json = buffer.clone().readString(UTF8);
//        Log.e(TAG, "response status " + response.header("status"));
        Log.e(TAG,  json);
//        try {
//            JSONObject jsonObject = new JSONObject(json);
//            if (jsonObject.has("status")){
//                int status = jsonObject.getInt("status");
//                if (status == 401){
//                    throw new TokenDeadException();
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        return response;
    }
}
