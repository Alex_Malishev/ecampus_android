package ru.aron_stoun.data_layer.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import ru.aron_stoun.data_layer.local.auth.AuthLocalStore;

/**
 * Created by jarvi on 02.07.2017.
 */

public class AuthenticationInterceptor implements Interceptor {

    private static final String TAG = AuthenticationInterceptor.class.getSimpleName();
    private AuthLocalStore authLocalStore;

    @Inject
    public AuthenticationInterceptor(AuthLocalStore authLocalStore) {
        this.authLocalStore = authLocalStore;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request original = chain.request();
//        Credentials credentials = authLocalStore.getCredentials();
        Request.Builder builder = original.newBuilder();
//        if (credentials != null) {
//            String token = (credentials.requestToken() != null ? "__RequestVerificationToken=" + credentials.requestToken(): "");
//            String ecampus =   (credentials.ecampusCookie() != null ? ";ecampus=" + credentials.ecampusCookie() : "");
//            String cookie = token + ecampus;
//            Log.e(TAG, "intercept: cookie " + cookie);
//            builder.header("Cookie", cookie);
//        }

        Request request = builder.build();

        Log.e(TAG, "intercept: " + request.url() + request.headers().toString());

        String json = bodyToString(request.body());
//        Log.e(TAG, "response status " + response.header("status"));
        Log.e(TAG, json);
//        if (request.body() != null)Log.e(TAG, "intercept: body " + request.body().toString());
        //        Log.e(TAG, "intercept: response " + response.body().string() );
        return chain.proceed(request);
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
