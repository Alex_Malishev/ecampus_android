package ru.aron_stoun.data_layer.utils;

import android.util.Log;

/**
 * Created by aron_stoun on 31.03.18.
 */

public class LogUtils {

    public static final boolean IS_DEGUG = true;

    public static void e(String TAG, String message){
        if (IS_DEGUG) Log.e(TAG, message);
    }

    public static void printStackTrace(Throwable throwable1) {
        if (IS_DEGUG) throwable1.printStackTrace();
    }
}
