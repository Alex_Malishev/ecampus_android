package ru.aron_stoun.data_layer.utils;

import java.io.IOException;

/**
 * Created by jarvi on 22.08.2017.
 */

public class NoConnectivityException extends IOException {

        @Override
        public String getMessage() {
            return "No connectivity exception";
        }
}
