package ru.aron_stoun.data_layer.injection;

import android.content.Context;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.meta.EntityModel;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;
import ru.aron_stoun.data_layer.BuildConfig;
import ru.aron_stoun.data_layer.local.DatabaseHelper;
import ru.aron_stoun.data_layer.model.db.Models;



/**
 * Created by jarvi on 18.07.2017.
 */
@Module
public class DatabaseModule {

    private static final int DATABASE_VERSION = 9;
    private Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    ReactiveEntityStore<Persistable> provideDatabase(Configuration configuration) {
        return ReactiveSupport.toReactiveStore(new EntityDataStore<Persistable>(configuration));
    }

    @Provides
    @Singleton
    EntityDataStore<Persistable> providerDataStore(Configuration configuration){
        return new EntityDataStore<Persistable>(configuration);
    }

    @Provides
    @Singleton
    Configuration provideConfiguration(DatabaseSource source, EntityModel model) {
        return new ConfigurationBuilder(source, model)
                .useDefaultLogging()
                .setWriteExecutor(Executors.newSingleThreadExecutor())
                .build();
    }

    @Provides
    @Singleton
    EntityModel provideModels() {
        return Models.DEFAULT;
    }

    @Provides
    @Singleton
    DatabaseSource provideDatabaseSource() {
        final DatabaseSource source = new DatabaseSource(context, Models.DEFAULT, DATABASE_VERSION);
        if (BuildConfig.DEBUG) {
            source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS);
        }
        return source;
    }

    @Provides
    @Singleton
    DatabaseHelper provideDatabaseHelper(ReactiveEntityStore<Persistable> dataStore, EntityDataStore<Persistable> entityDataStore){
        return new DatabaseHelper(dataStore, entityDataStore);
    }
}
