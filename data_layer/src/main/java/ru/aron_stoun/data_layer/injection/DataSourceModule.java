package ru.aron_stoun.data_layer.injection;

import dagger.Module;
import dagger.Provides;
import ru.aron_stoun.data_layer.repository.auth.AuthRepository;
import ru.aron_stoun.data_layer.repository.schedule.ScheduleRepository;
import ru.aron_stoun.data_layer.repository.subjects.SubjectsRepository;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthRepository;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleRepository;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectsRepository;

@Module(includes = NetModule.class)
public class DataSourceModule {

    public DataSourceModule() {
    }


    @Provides
    IAuthRepository provideIAuthRepository(AuthRepository authRepository){
        return authRepository;
    }

    @Provides
    ISubjectsRepository provideISubjectRepository(SubjectsRepository subjectsRepository){
        return subjectsRepository;
    }

    @Provides
    IScheduleRepository provideIScheduleRepository(ScheduleRepository scheduleRepository){
        return scheduleRepository;
    }
}
