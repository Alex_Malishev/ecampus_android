package ru.aron_stoun.data_layer.injection;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.aron_stoun.data_layer.local.DatabaseHelper;
import ru.aron_stoun.data_layer.local.auth.AuthLocalStore;
import ru.aron_stoun.data_layer.utils.AuthenticationInterceptor;
import ru.aron_stoun.data_layer.utils.ConnectionInterceptor;
import ru.aron_stoun.data_layer.utils.NetworkUtils;
import ru.aron_stoun.data_layer.utils.ServiceGenerator;
import ru.aron_stoun.data_layer.utils.TokenStatusInterceptor;


/**
 * Created by jarvi on 21.07.2017.
 */

@Module(includes = DatabaseModule.class)
public class NetModule {

    public static final String API_BASE_URL = "http://194.67.193.142/api/v1/";
    public static final String API_TEST_URL = "https://ecampus.ncfu.ru/";
    public static final String IMAGE_URL = "http://194.67.193.142/";
    public static final String IMAGE_TEST_URL = "http://goraptekatest.routeam.ru/";

    public NetModule() {
    }

    @Provides
    @Singleton
    Retrofit.Builder provideBuilder(){
        return new Retrofit.Builder()
                .baseUrl(API_TEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @Provides
    @Singleton
    OkHttpClient.Builder provideOkHttpClient(){
        return NetworkUtils.getUnsafeOkHttpClient();
    }

    @Provides
    @Singleton
    ServiceGenerator provideServiceGenerator(OkHttpClient.Builder okBuilder, Retrofit.Builder retrofit,
                                             AuthenticationInterceptor authenticationInterceptor,
                                             TokenStatusInterceptor tokenStatusInterceptor,
                                             ConnectionInterceptor connectionInterceptor){
        return new ServiceGenerator(okBuilder, retrofit, authenticationInterceptor, tokenStatusInterceptor, connectionInterceptor);
    }

    @Provides
    @Singleton
    AuthenticationInterceptor provideAuthenticationInterceptor(AuthLocalStore authLocalStore){
        return new AuthenticationInterceptor(authLocalStore);
    }

    @Provides
    @Singleton
    AuthLocalStore provideAuthLocalStore(DatabaseHelper databaseHelper){
        return new AuthLocalStore(databaseHelper);
    }

    @Provides
    @Singleton
    TokenStatusInterceptor provideTokenStatusInterceptor(){
        return new TokenStatusInterceptor();
    }

    @Provides
    @Singleton
    ConnectionInterceptor provideConnectionInterceptor(){
        return new ConnectionInterceptor();
    }



}
