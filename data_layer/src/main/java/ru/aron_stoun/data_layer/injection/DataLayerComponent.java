package ru.aron_stoun.data_layer.injection;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.aron_stoun.data_layer.local.DatabaseHelper;
import ru.aron_stoun.data_layer.utils.AuthenticationInterceptor;
import ru.aron_stoun.data_layer.utils.ConnectionInterceptor;
import ru.aron_stoun.data_layer.utils.ServiceGenerator;
import ru.aron_stoun.data_layer.utils.TokenStatusInterceptor;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthRepository;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleRepository;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectsRepository;

@Component(modules = {DataLayerModule.class, DataSourceModule.class})
@Singleton
public interface DataLayerComponent {

    Context context();

    IAuthRepository authRepository();
    ISubjectsRepository subjectsRepository();
    IScheduleRepository scheduleRepository();

}
