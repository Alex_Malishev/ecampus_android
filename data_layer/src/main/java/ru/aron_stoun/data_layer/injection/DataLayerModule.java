package ru.aron_stoun.data_layer.injection;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {NetModule.class, DatabaseModule.class})
public class DataLayerModule {

    private Context context;

    public DataLayerModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext(){
        return context;
    }
}
