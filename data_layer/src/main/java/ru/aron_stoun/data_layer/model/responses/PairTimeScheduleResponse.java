package ru.aron_stoun.data_layer.model.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.PairTime;


public class PairTimeScheduleResponse {

    @SerializedName("WeekNumber")
    private int weekNumber;
    @SerializedName("PairsTime")
    private ArrayList<PairTime> pairTimes;
    @SerializedName("AcademicDay")
    private boolean isAcademicDay;
    @SerializedName("PairNumber")
    private String pairNumber;

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public ArrayList<PairTime> getPairTimes() {
        return pairTimes;
    }

    public void setPairTimes(ArrayList<PairTime> pairTimes) {
        this.pairTimes = pairTimes;
    }

    public boolean isAcademicDay() {
        return isAcademicDay;
    }

    public void setAcademicDay(boolean academicDay) {
        isAcademicDay = academicDay;
    }

    public String getPairNumber() {
        return pairNumber;
    }

    public void setPairNumber(String pairNumber) {
        this.pairNumber = pairNumber;
    }
}
