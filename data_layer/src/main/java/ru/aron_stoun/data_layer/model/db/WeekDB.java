package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;

@Entity
public interface WeekDB extends Parcelable, Persistable{

    @Key
    String dateBegin();
    String dateEnd();
    int number();
    boolean isActual();

}
