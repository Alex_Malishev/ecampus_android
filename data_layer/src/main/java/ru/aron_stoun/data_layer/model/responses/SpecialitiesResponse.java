package ru.aron_stoun.data_layer.model.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.Speciality;


public class SpecialitiesResponse {

    private ArrayList<Speciality> specialities;
    private int portionSize;

    @SerializedName("Kod_cart")
    private int kodcart;

    public ArrayList<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(ArrayList<Speciality> specialities) {
        this.specialities = specialities;
    }

    public int getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(int portionSize) {
        this.portionSize = portionSize;
    }

    public int getKodcart() {
        return kodcart;
    }

    public void setKodcart(int kodcart) {
        this.kodcart = kodcart;
    }
}
