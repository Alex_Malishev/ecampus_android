package ru.aron_stoun.data_layer.model.responses;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.Course;


public class CoursesResponse {

    private ArrayList<Course> courses;

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
}
