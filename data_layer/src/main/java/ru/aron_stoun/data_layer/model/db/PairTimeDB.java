package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;

@Entity
public interface PairTimeDB extends Parcelable, Persistable {

    @Key
    int id();
    String name();
    int hourStart();
    int minuteStart();
    int hourEnd();
    int minuteEnd();
    int duration();
    String startTime();
    String endTime();
    boolean isActual();
}
