package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface ScheduleLessonDB extends Parcelable, Persistable {

    @Key
    int id();

    int lessonDayId();

    String discipline();

    String timeBegin();

    String timeEnd();

    @ManyToOne(cascade = {CascadeAction.NONE})
    ClassroomDB classroom();

    String lessonType();

    int pairNumberStart();

    int pairNumberEnd();

    @ManyToOne(cascade = {CascadeAction.NONE})
    TeacherDB teacher();

    @OneToMany(mappedBy = "lesson", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<GroupDB> groups();

    boolean isActual();

    String lessonName();


    @ManyToOne(cascade = CascadeAction.NONE)
    WeekdayDB weekday();

}
