package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface TermDb extends Parcelable, Persistable {

    @Key
    int id();
    int parentId();
    String name();
    boolean isActual();
    String termTypeName();

    @OneToMany(mappedBy = "term", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<CourseDB> courses();

    @ManyToOne(cascade = {CascadeAction.NONE})
    AcademicYearDB academicYear();
}
