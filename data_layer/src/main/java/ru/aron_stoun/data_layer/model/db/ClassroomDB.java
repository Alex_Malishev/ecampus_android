package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface ClassroomDB extends Parcelable, Persistable {

    @Key
    int id();
    String name();

    @OneToMany(mappedBy = "classroom", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<ScheduleLessonDB> lessons();
}
