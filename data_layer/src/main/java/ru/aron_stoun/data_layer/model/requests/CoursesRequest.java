package ru.aron_stoun.data_layer.model.requests;

import com.google.gson.annotations.SerializedName;

public class CoursesRequest {

    @SerializedName("studentId")
    private Integer specialityId;
    private Integer termId;

    public CoursesRequest(Integer specialityId, Integer termId) {
        this.specialityId = specialityId;
        this.termId = termId;
    }

    public Integer getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(Integer specialityId) {
        this.specialityId = specialityId;
    }

    public Integer getTermId() {
        return termId;
    }

    public void setTermId(Integer termId) {
        this.termId = termId;
    }
}
