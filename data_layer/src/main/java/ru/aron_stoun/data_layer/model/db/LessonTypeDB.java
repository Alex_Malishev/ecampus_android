package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface LessonTypeDB extends Parcelable, Persistable{

    @Key
    int id();
    int parentId();
    String name();
    boolean schoolType();

    @OneToMany(mappedBy = "lessonType", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<LessonDB> lessons();

    @ManyToOne(cascade = {CascadeAction.NONE})
    CourseDB course();

}
