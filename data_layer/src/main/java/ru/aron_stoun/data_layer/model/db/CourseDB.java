package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface CourseDB extends Parcelable, Persistable {

    @Key
    int id();
    int parentId();
    String name();
    int maxRating();
    double currentRating();

    @OneToMany(mappedBy = "course", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<LessonTypeDB> lessonTypes();

    @ManyToOne(cascade = {CascadeAction.NONE})
    TermDb term();
}
