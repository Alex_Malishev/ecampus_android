package ru.aron_stoun.data_layer.model.requests;

import com.google.gson.annotations.SerializedName;

public class ScheduleRequest {

    private String date;

    @SerializedName("Id")
    private int id;

    private int targetType;

    public ScheduleRequest() {
    }

    public ScheduleRequest(String date, int id, int targetType) {
        this.date = date;
        this.id = id;
        this.targetType = targetType;
    }


}
