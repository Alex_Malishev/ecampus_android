package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface SpecialityDB extends Persistable, Parcelable {

    @Key
    int id();

    String name();

    @OneToMany(mappedBy = "speciality", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<AcademicYearDB> academicYears();
}
