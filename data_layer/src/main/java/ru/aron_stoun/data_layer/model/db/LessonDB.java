package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;

@Entity
public interface LessonDB extends Parcelable, Persistable {

    @Key
    int id();
    int parentId();
    int attendance();
    String lessonDate();
    double gainedScore();
    String gradeText();
    boolean isCheckpoint();
    double lostScore();
    double penaltyScore();
    String room();
    String shortName();
    String teacher();

    @ManyToOne(cascade = {CascadeAction.NONE})
    LessonTypeDB lessonType();
}
