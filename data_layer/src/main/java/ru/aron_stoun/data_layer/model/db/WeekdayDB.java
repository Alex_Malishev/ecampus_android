package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.Date;
import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface WeekdayDB extends Parcelable, Persistable{

    @Key @Generated
    int id();

    String name();

    Date weekDate();

    @OneToMany(mappedBy = "weekday", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<ScheduleLessonDB> lessons();
}
