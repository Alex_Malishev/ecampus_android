package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;

@Entity
public interface GroupDB extends Parcelable, Persistable{

    @Key
    int id();
    String name();
    String subgroup();

    @ManyToOne(cascade = CascadeAction.NONE)
    ScheduleLessonDB lesson();
}
