package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by aron_stoun on 25.03.18.
 */
@Entity
public interface BaseModel extends Parcelable, Persistable{
    @Key @Generated
    int id();
    String name();
}
