package ru.aron_stoun.data_layer.model.requests;

public class LessonsRequest {

    private Integer specialityId;
    private Integer lessonTypeId;

    public LessonsRequest(Integer specialityId, Integer lessonTypeId) {
        this.specialityId = specialityId;
        this.lessonTypeId = lessonTypeId;
    }

    public Integer getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(Integer specialityId) {
        this.specialityId = specialityId;
    }

    public Integer getLessonTypeId() {
        return lessonTypeId;
    }

    public void setLessonTypeId(Integer lessonTypeId) {
        this.lessonTypeId = lessonTypeId;
    }
}
