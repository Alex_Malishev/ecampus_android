package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

@Entity
public interface Credentials extends Persistable, Parcelable {


    @Key @Generated
    int id();
    String login();
    String password();

}
