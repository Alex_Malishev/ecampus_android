package ru.aron_stoun.data_layer.model.db;

import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public interface AcademicYearDB extends Persistable, Parcelable {

    @Key
    int id();
    int parentId();
    String name();
    String courseTypeName();

    @OneToMany(mappedBy = "academicYear", cascade = {CascadeAction.SAVE, CascadeAction.DELETE})
    List<TermDb> terms();

    @ManyToOne(cascade = {CascadeAction.NONE})
    SpecialityDB speciality();

}
