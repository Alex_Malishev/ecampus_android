package ru.aron_stoun.data_layer.repository.schedule;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import ru.aron_stoun.data_layer.local.schedule.ScheduleLocalStore;
import ru.aron_stoun.data_layer.model.db.WeekDB;
import ru.aron_stoun.data_layer.model.requests.ScheduleRequest;
import ru.aron_stoun.data_layer.model.responses.PairTimeScheduleResponse;
import ru.aron_stoun.data_layer.remote.ScheduleDataSource;
import ru.aron_stoun.data_layer.utils.LogUtils;
import ru.aron_stoun.data_layer.utils.NoConnectivityException;
import ru.aron_stoun.data_layer.utils.ServiceGenerator;
import ru.aron_stoun.domain_layer.DateUtils;
import ru.aron_stoun.domain_layer.models.MyScheduleWeeks;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.models.Week;
import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleRepository;


public class ScheduleRepository implements IScheduleRepository {


    private static final String TAG = ScheduleRepository.class.getSimpleName();
    private ScheduleLocalStore scheduleLocalStore;
    private ScheduleDataSource scheduleDataSource;

    @Inject
    public ScheduleRepository(ScheduleLocalStore scheduleLocalStore, ServiceGenerator serviceGenerator) {
        this.scheduleLocalStore = scheduleLocalStore;
        this.scheduleDataSource = serviceGenerator.createService(ScheduleDataSource.class);
    }

    @Override
    public Observable<List<Weekday>> getMySchedule(Date date, int id, int targetType) {
        ScheduleRequest scheduleRequest = new ScheduleRequest(DateUtils.formatDate(date), id, targetType);
        return scheduleDataSource.getSchedule(scheduleRequest)
                .map(onSuccessSchedule())
                .onErrorResumeNext(onErrorMySchedule(date));
    }

    @Override
    public Observable<MyScheduleWeeks> getMyScheduleWeeks() {
        return scheduleDataSource.getMyStudy()
                .map(mapMyScheduleWeek())
                .onErrorResumeNext(onErrorScheduleWeeks());
    }

    @Override
    public Observable<List<PairTime>> getPairTimeSchedule() {
        return scheduleDataSource.getPairTimeSchedule()
                .map(onSuccessPairTimeSchedule())
                .onErrorResumeNext(onErrorPairTimeSchedule());
    }

    /**
     * Parse {@link MyScheduleWeeks} from html response
     * @return {@link MyScheduleWeeks}
     */
    private Function<ResponseBody, MyScheduleWeeks> mapMyScheduleWeek(){
        return responseBody -> {
            String response = responseBody.string();
            responseBody.close();
            String temp1 = response.substring(response.lastIndexOf("var viewModel") + 1);
            String temp2 = temp1.substring(temp1.indexOf("=") + 1).trim();
            String temp3 = temp2.split("</script>")[0];
            String temp4 = temp3.replaceAll("JSON.parse\\(", "")
                    .replaceAll("\\)", "")
                    .replace("\\\"", "");

            JSONObject jsonObject = new JSONObject(temp4);
            LogUtils.e(TAG, " my schedule json is - " + jsonObject.toString());
            Gson gson = new Gson();
            MyScheduleWeeks myScheduleWeeks = gson.fromJson(jsonObject.toString(), MyScheduleWeeks.class);
            Observable.just(myScheduleWeeks.getWeeks())
                    .map(ScheduleMapper.ToDB::mapWeeks)
                    .subscribe(weekDBS -> scheduleLocalStore.saveWeeks(weekDBS, myScheduleWeeks.getCurrentWeekIndex()));

            return myScheduleWeeks;
        };
    }


    /**
     * Get {@link MyScheduleWeeks} from database when there is no internet connection
     * @return {@link Function} with {@link ObservableSource<MyScheduleWeeks>}
     */
    private Function<Throwable, ObservableSource<MyScheduleWeeks>> onErrorScheduleWeeks(){
        return throwable -> {
            LogUtils.printStackTrace(throwable);
            if (throwable instanceof NoConnectivityException){
                return scheduleLocalStore.getWeeks().map(weekDBS -> {
                    int currentIndex = 0;
                    Date today = new Date();
                    for (int i = 0; i < weekDBS.size(); i++) {
                        WeekDB weekDB = weekDBS.get(i);
                        if (!Objects.requireNonNull(DateUtils.dateFromString(weekDB.dateBegin())).after(today)
                                && !Objects.requireNonNull(DateUtils.dateFromString(weekDB.dateEnd())).before(today)) {
                            currentIndex = i;
                            break;
                        }
                    }
                    ArrayList<Week> weeks = ScheduleMapper.FromDB.unmapWeeks(weekDBS);
                    MyScheduleWeeks myScheduleWeeks = new MyScheduleWeeks();
                    myScheduleWeeks.setWeeks(weeks);
                    myScheduleWeeks.setCurrentWeekIndex(currentIndex);
                    myScheduleWeeks.setModel(myScheduleWeeks.new Model());
                    return myScheduleWeeks;
                });
            }
            return Observable.just(new MyScheduleWeeks());
        };
    }

    /**
     * Save pair time schedule to database and return list of {@link PairTime}
     * @return {@link Function} with {@link List<PairTime>}
     */
    private Function<PairTimeScheduleResponse, List<PairTime>> onSuccessPairTimeSchedule(){
        return pairTimeScheduleResponse -> {
            Observable.just(pairTimeScheduleResponse.getPairTimes())
                    .map(ScheduleMapper.ToDB::mapPairTimeSchedule)
                    .subscribe(pairTimeDBEntities -> scheduleLocalStore.savePairTimeSchedule(pairTimeDBEntities));
            return pairTimeScheduleResponse.getPairTimes();
        };
    }

    /**
     * Get list of {@link PairTime} from database when there is no internet connection
     * @return {@link Function} with {@link List<PairTime>}
     */
    private Function<Throwable, ObservableSource<List<PairTime>>> onErrorPairTimeSchedule(){
        return throwable -> {
            if (throwable instanceof NoConnectivityException){
                return scheduleLocalStore.getPairTimeSchedule().map(ScheduleMapper.FromDB::unmapPairTimeSchedule);
            }
            return Observable.just(new ArrayList<>());
        };
    }


    /**
     * Save schedule to database and return list of {@link Weekday}
     * @return {@link Function} with {@link List<Weekday>}
     */
    private Function<List<Weekday>, List<Weekday>> onSuccessSchedule(){
        return weekdays -> {
            Observable.just(weekdays)
                    .map(ScheduleMapper.ToDB::mapWeekday)
                    .subscribe(weekdayDBS -> scheduleLocalStore.saveScheduleWeek(weekdayDBS));
            return weekdays;
        };
    }

    /**
     * Get schedule from database when there is no internet connection
     * @param dateStart {@link Date} date of first day in a schedule of week
     * @return {@link Function} with {@link List<Weekday>}
     */
    private Function<Throwable, ObservableSource<List<Weekday>>> onErrorMySchedule(Date dateStart) {
        return throwable -> {
            if (throwable instanceof NoConnectivityException){
                return scheduleLocalStore.getScheduleWeek(dateStart).map(ScheduleMapper.FromDB::unmapWeekday);
            }
            return Observable.just(new ArrayList<>());
        };
    }
}
