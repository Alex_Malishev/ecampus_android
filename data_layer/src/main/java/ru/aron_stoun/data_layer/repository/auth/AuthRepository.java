package ru.aron_stoun.data_layer.repository.auth;

import java.net.HttpCookie;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ru.aron_stoun.data_layer.local.auth.AuthLocalStore;
import ru.aron_stoun.data_layer.model.db.Credentials;
import ru.aron_stoun.data_layer.remote.AuthDataSource;
import ru.aron_stoun.data_layer.utils.NoConnectivityException;
import ru.aron_stoun.data_layer.utils.ServiceGenerator;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthRepository;


public class AuthRepository implements IAuthRepository {


    private final AuthDataSource authDataSource;
    private AuthLocalStore authLocalStore;
    private ServiceGenerator serviceGenerator;

    @Inject
    public AuthRepository(ServiceGenerator serviceGenerator, AuthLocalStore authLocalStore) {
        this.serviceGenerator = serviceGenerator;
        this.authDataSource = serviceGenerator.createService(AuthDataSource.class);
        this.authLocalStore = authLocalStore;
    }


    @Override
    public Observable<Boolean> login(String requestToken, String login, String password, boolean rememberMe) {
        return authDataSource.login(requestToken, login, password, rememberMe)
                .map(isLoginSuccessful(login, password))
                .onErrorResumeNext(onLoginError());
    }

    @Override
    public Observable<String> getRequestToken() {
        return authDataSource.getRequestVerificationToken()
                .map(getToken())
                .onErrorResumeNext(onTokenError());
    }

    @Override
    public Observable<ru.aron_stoun.domain_layer.models.Credentials> checkUserCredentialsExist() {
        Credentials credentialsDb = authLocalStore.getCredentials();
        ru.aron_stoun.domain_layer.models.Credentials credentials = null;
        if (credentialsDb != null) {
            credentials = new ru.aron_stoun.domain_layer.models.Credentials();
            credentials.setLogin(credentialsDb.login());
            credentials.setPassword(credentialsDb.password());
        }

        return credentials != null ? Observable.just(credentials) : null;
    }


    /**
     * Method gets the RequestVerificationToken from html response
     * @return {@link Function<>} with RequestVerificationToken {@link String}
     */
    private Function<Response<ResponseBody>, String> getToken() {
        return (response) -> {
            String token;
            if (response.body() != null) {
                token = getTokenFromResponse(response.body().string());
                response.body().close();
                return token;
            }
            return "";
        };
    }

    /**
     * Method parses RequestVerificationToken from html response
     * @param body Response body
     * @return RequestVerificationToken {@link String}
     */
    private String getTokenFromResponse(String body) {
        String[] firstStep = body.split("RequestVerificationToken\" type=\"hidden\" value=\"");
        String[] secondStep = firstStep[1].split("\" /> ", -1);
        return secondStep[0];
    }


    /**
     * Method search auth cookie "ecampus" in cookies list and if it exists saves user's credentials in database
     * @param login {@link String} user's login
     * @param password {@link String} user's password
     * @return {@link Function<>}
     */
    private Function<Response<ResponseBody>, Boolean> isLoginSuccessful(String login, String password) {
        return responseBodyResponse -> {
            List<HttpCookie> httpCookieList = serviceGenerator.getCookies();
            for (HttpCookie cookie : httpCookieList) {
                if (cookie.getName().equals("ecampus") & !cookie.getValue().isEmpty()) {
                    authLocalStore.saveCredentials(login, password);
                    return true;
                }
            }
            return false;
        };
    }

    /**
     * Method checks if login error due to no connectivity
     * @return {@link Function<>} with {@link ObservableSource<Boolean>}
     */
    private Function<Throwable, ObservableSource<Boolean>> onLoginError() {
        return throwable1 -> {
            if (throwable1 instanceof NoConnectivityException) {
                return Observable.just(true);
            }
            return Observable.just(false);
        };

    }

    /**
     * When token doesn't exist return an empty string
     * @return  {@link String}
     */
    private Function<Throwable, ObservableSource<String>> onTokenError() {
        return throwable1 -> Observable.just("");

    }
}
