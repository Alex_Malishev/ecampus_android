package ru.aron_stoun.data_layer.repository.subjects;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;
import ru.aron_stoun.data_layer.model.db.AcademicYearDB;
import ru.aron_stoun.data_layer.model.db.AcademicYearDBEntity;
import ru.aron_stoun.data_layer.model.db.CourseDB;
import ru.aron_stoun.data_layer.model.db.CourseDBEntity;
import ru.aron_stoun.data_layer.model.db.LessonDB;
import ru.aron_stoun.data_layer.model.db.LessonDBEntity;
import ru.aron_stoun.data_layer.model.db.LessonTypeDB;
import ru.aron_stoun.data_layer.model.db.LessonTypeDBEntity;
import ru.aron_stoun.data_layer.model.db.SpecialityDB;
import ru.aron_stoun.data_layer.model.db.SpecialityDBEntity;
import ru.aron_stoun.data_layer.model.db.TermDb;
import ru.aron_stoun.data_layer.model.db.TermDbEntity;
import ru.aron_stoun.domain_layer.models.AcademicYear;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Lesson;
import ru.aron_stoun.domain_layer.models.LessonType;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.domain_layer.models.Term;

import static ru.aron_stoun.data_layer.repository.subjects.SubjectMapper.FromDB.unmapAcademicYears;
import static ru.aron_stoun.data_layer.repository.subjects.SubjectMapper.ToDB.mapAcademicYears;

public class SubjectMapper {


    public static Function<List<Speciality>, List<SpecialityDB>> mapSpecialitiesForDb = specialities1 -> {
        List<SpecialityDB> specialityDBS = new ArrayList<>();
        for (Speciality s : specialities1) {
            SpecialityDBEntity specialityDBEntity = new SpecialityDBEntity();
            specialityDBEntity.id(s.getId());
            specialityDBEntity.name(s.getName());
            specialityDBEntity.academicYears(mapAcademicYears(s.getAcademicYears()));
            specialityDBS.add(specialityDBEntity);
        }
        return specialityDBS;
    };

    public static Function<List<SpecialityDB>, List<Speciality>> mapSpecialitiesFromDb = specialities1 -> {
        List<Speciality> specialities = new ArrayList<>();
        for (SpecialityDB s : specialities1) {
            Speciality speciality = new Speciality();
            speciality.setId(s.id());
            speciality.setName(s.name());
            speciality.setAcademicYears(unmapAcademicYears(s.academicYears()));
            specialities.add(speciality);
        }
        return specialities;
    };


    /**
     * The class prepares model to store in database
     */
    public static class ToDB {

        static ArrayList<AcademicYearDB> mapAcademicYears(ArrayList<AcademicYear> academicYears) {
            if (academicYears == null) return new ArrayList<>();
            ArrayList<AcademicYearDB> academicYearDBS = new ArrayList<>();
            for (AcademicYear academicYear : academicYears) {
                AcademicYearDBEntity academicYearDBEntity = new AcademicYearDBEntity();
                academicYearDBEntity.id(academicYear.getId());
                academicYearDBEntity.parentId(academicYear.getParentId());
                academicYearDBEntity.courseTypeName(academicYear.getCourseTypeName());
                academicYearDBEntity.name(academicYear.getName());
                academicYearDBEntity.terms(mapTerms(academicYear.getTerms()));
                academicYearDBS.add(academicYearDBEntity);

            }
            return academicYearDBS;
        }

        public static ArrayList<TermDb> mapTerms(ArrayList<Term> terms) {
            if (terms == null) return new ArrayList<>();
            ArrayList<TermDb> termDbs = new ArrayList<>();
            for (Term term : terms) {
                TermDbEntity termDbEntity = new TermDbEntity();
                termDbEntity.id(term.getId());
                termDbEntity.parentId(term.getParentId());
                termDbEntity.name(term.getName());
                termDbEntity.setActual(term.isCurrent());
                termDbEntity.termTypeName(term.getTermTypeName());
                termDbEntity.courses(mapCourses(term.getCourses()));
                termDbs.add(termDbEntity);
            }
            return termDbs;
        }

        public static ArrayList<CourseDB> mapCourses(ArrayList<Course> courses) {
            if (courses == null) return new ArrayList<>();
            ArrayList<CourseDB> courseDBS = new ArrayList<>();
            for (Course course : courses) {
                CourseDBEntity courseDBEntity = new CourseDBEntity();
                courseDBEntity.id(course.getId());
                courseDBEntity.parentId(course.getParentId());
                courseDBEntity.name(course.getName());
                courseDBEntity.maxRating(course.getMaxRating());
                courseDBEntity.currentRating(course.getCurrentRating());
                courseDBEntity.lessonTypes(mapLessonTypes(course.getLessonTypes()));
                courseDBS.add(courseDBEntity);
            }
            return courseDBS;
        }

        public static ArrayList<LessonTypeDB> mapLessonTypes(ArrayList<LessonType> lessonTypes) {
            if (lessonTypes == null) return new ArrayList<>();
            ArrayList<LessonTypeDB> lessonTypeDBS = new ArrayList<>();
            for (LessonType lessonType : lessonTypes) {
                LessonTypeDBEntity lessonTypeDBEntity = new LessonTypeDBEntity();
                lessonTypeDBEntity.id(lessonType.getId());
                lessonTypeDBEntity.parentId(lessonType.getParentId());
                lessonTypeDBEntity.name(lessonType.getName());
                lessonTypeDBEntity.schoolType(lessonType.isSchoolType());
                lessonTypeDBEntity.lessons(mapLessons(lessonType.getLessons()));
                lessonTypeDBS.add(lessonTypeDBEntity);
            }
            return lessonTypeDBS;
        }

        public static ArrayList<LessonDB> mapLessons( ArrayList<Lesson> lessons) {
            if (lessons == null) return new ArrayList<>();
            ArrayList<LessonDB> lessonDBS = new ArrayList<>();
            for (Lesson lesson : lessons) {
                LessonDBEntity lessonDBEntity = new LessonDBEntity();
                lessonDBEntity.id(lesson.getId());
                lessonDBEntity.parentId(lesson.getParentId());
                lessonDBEntity.attendance(lesson.getAttendance());
                lessonDBEntity.lessonDate(lesson.getDate());
                lessonDBEntity.gainedScore(lesson.getGainedScore());
                lessonDBEntity.gradeText(lesson.getGradeText());
                lessonDBEntity.setCheckpoint(lesson.isCheckpoint());
                lessonDBEntity.lostScore(lesson.getLostScore());
                lessonDBEntity.penaltyScore(lesson.getPenaltyScore());
                lessonDBEntity.room(lesson.getRoom());
                lessonDBEntity.shortName(lesson.getShortName());
                lessonDBEntity.teacher(lesson.getTeacher());
                lessonDBS.add(lessonDBEntity);
            }
            return lessonDBS;
        }
    }

    /**
     * This class parse models from database
     * */
    public static class FromDB {

        public static ArrayList<AcademicYear> unmapAcademicYears(List<AcademicYearDB> academicYearsDBS) {
            if (academicYearsDBS == null) return new ArrayList<>();
            ArrayList<AcademicYear> academicYears = new ArrayList<>();
            for (AcademicYearDB academicYearDB : academicYearsDBS) {
                AcademicYear academicYear = new AcademicYear();
                academicYear.setId(academicYearDB.id());
                academicYear.setParentId(academicYearDB.parentId());
                academicYear.setCourseTypeName(academicYearDB.courseTypeName());
                academicYear.setName(academicYearDB.name());
                academicYear.setTerms(unmapTerms(academicYearDB.terms()));
                academicYears.add(academicYear);

            }
            return academicYears;
        }

        public static ArrayList<Term> unmapTerms(List<TermDb> termsDbs) {
            if (termsDbs == null )return new ArrayList<>();
            ArrayList<Term> terms = new ArrayList<>();
            for (TermDb termdb : termsDbs) {
                Term term = new Term();
                term.setId(termdb.id());
                term.setParentId(termdb.parentId());
                term.setName(termdb.name());
                term.setCurrent(termdb.isActual());
                term.setTermTypeName(termdb.termTypeName());
                term.setCourses(unmapCourses(termdb.courses()));
                terms.add(term);
            }
            return terms;
        }

        public static ArrayList<Course> unmapCourses(List<CourseDB> courseDBS) {
            if (courseDBS == null) return new ArrayList<>();
            ArrayList<Course> courses = new ArrayList<>();
            for (CourseDB courseDB : courseDBS) {
                Course course = new Course();
                course.setId(courseDB.id());
                course.setParentId(courseDB.parentId());
                course.setName(courseDB.name());
                course.setMaxRating(courseDB.maxRating());
                course.setCurrentRating(courseDB.currentRating());
                course.setLessonTypes(unmapLessonTypes(courseDB.lessonTypes()));
                courses.add(course);
            }
            return courses;
        }

        public static ArrayList<LessonType> unmapLessonTypes(List<LessonTypeDB> lessonTypeDBS) {
            if (lessonTypeDBS == null) return new ArrayList<>();
            ArrayList<LessonType> lessonTypes = new ArrayList<>();
            for (LessonTypeDB lessonTypeDB : lessonTypeDBS) {
                LessonType lessonType = new LessonType();
                lessonType.setId(lessonTypeDB.id());
                lessonType.setParentId(lessonTypeDB.parentId());
                lessonType.setName(lessonTypeDB.name());
                lessonType.setSchoolType(lessonTypeDB.schoolType());
                lessonType.setLessons(unmapLessons(lessonTypeDB.lessons()));
                lessonTypes.add(lessonType);
            }
            return lessonTypes;
        }

        public static ArrayList<Lesson> unmapLessons(List<LessonDB> lessonDBS) {
            if (lessonDBS == null) return new ArrayList<>();
            ArrayList<Lesson> lessons = new ArrayList<>();
            for (LessonDB lessonDB : lessonDBS) {
                Lesson lesson = new Lesson();
                lesson.setId(lessonDB.id());
                lesson.setParentId(lessonDB.parentId());
                lesson.setAttendance(lessonDB.attendance());
                lesson.setDate(lessonDB.lessonDate());
                lesson.setGainedScore(lessonDB.gainedScore());
                lesson.setGradeText(lessonDB.gradeText());
                lesson.setCheckpoint(lessonDB.isCheckpoint());
                lesson.setLostScore(lessonDB.lostScore());
                lesson.setPenaltyScore(lessonDB.penaltyScore());
                lesson.setRoom(lessonDB.room());
                lesson.setShortName(lessonDB.shortName());
                lesson.setTeacher(lessonDB.teacher());
                lessons.add(lesson);
            }
            return lessons;
        }
    }


}
