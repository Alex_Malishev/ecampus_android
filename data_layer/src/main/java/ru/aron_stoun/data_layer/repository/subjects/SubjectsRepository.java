package ru.aron_stoun.data_layer.repository.subjects;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

import ru.aron_stoun.data_layer.local.subjects.SubjectsLocalStore;
import ru.aron_stoun.data_layer.model.requests.CoursesRequest;
import ru.aron_stoun.data_layer.model.requests.LessonsRequest;
import ru.aron_stoun.data_layer.model.responses.CoursesResponse;
import ru.aron_stoun.data_layer.remote.SubjectsDataSource;
import ru.aron_stoun.data_layer.utils.LogUtils;
import ru.aron_stoun.data_layer.utils.NoConnectivityException;
import ru.aron_stoun.data_layer.utils.ServiceGenerator;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Lesson;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectsRepository;

import static ru.aron_stoun.data_layer.repository.subjects.SubjectMapper.ToDB.mapLessons;

public class SubjectsRepository implements ISubjectsRepository {

    private SubjectsDataSource subjectsDataSource;
    private SubjectsLocalStore subjectsLocalStore;

    @Inject
    public SubjectsRepository(ServiceGenerator serviceGenerator, SubjectsLocalStore subjectsLocalStore) {
        this.subjectsDataSource = serviceGenerator.createService(SubjectsDataSource.class);
        this.subjectsLocalStore = subjectsLocalStore;
    }

    @Override
    public Observable<List<Speciality>> getSpecialities() {
        return subjectsDataSource.getSpecialities()
                .map(onSuccessSpecialities())
                .onErrorResumeNext(onErrorSpecialities());
    }

    @Override
    public Observable<List<Course>> getCourses(int specialityId, int termId) {
        CoursesRequest coursesRequest = new CoursesRequest(specialityId, termId);
        return subjectsDataSource.getCourses(coursesRequest)
                .map(onSuccessCourses(termId))
                .onErrorResumeNext(onErrorCourses(specialityId, termId));
    }

    @Override
    public Observable<List<Lesson>> getLessons(int specialityId, int lessonTypeId) {
        LessonsRequest lessonsRequest = new LessonsRequest(specialityId, lessonTypeId);
        return subjectsDataSource.getLessons(lessonsRequest)
                .map(onSuccessLessons())
                .onErrorResumeNext(onErrorLessons(specialityId, lessonTypeId));
    }


    /**
     * In case of no connectivity error method returns specialities from database,
     * otherwise method returns an empty list
     */
    private Function<Throwable, ObservableSource<List<Speciality>>> onErrorSpecialities() {
        return throwable1 -> {
            LogUtils.printStackTrace(throwable1);
            if (throwable1 instanceof NoConnectivityException) {
                return subjectsLocalStore.getSpecialities().map(SubjectMapper.mapSpecialitiesFromDb);
            }
            return Observable.just(new ArrayList<>());
        };
    }

    /**
     * This method saves specialities to database
     */
    private Function<List<Speciality>, List<Speciality>> onSuccessSpecialities() {
        return specialities -> {
            Observable.just(specialities)
                    .map(SubjectMapper.mapSpecialitiesForDb)
                    .doOnNext(specialityDBS -> subjectsLocalStore.saveSpecialities(specialityDBS))
                    .subscribe();
            return specialities;
        };
    }

    /**
     * This method saves courses to database
     * */
    private Function<CoursesResponse, List<Course>> onSuccessCourses(int termId) {
        return coursesResponse -> {
            Observable.just(coursesResponse.getCourses())
                    .map(SubjectMapper.ToDB::mapCourses)
                    .doOnNext(courseDBS -> subjectsLocalStore.saveCourses(termId,courseDBS))
                    .subscribe();
            return coursesResponse.getCourses();
        };
    }

    /**
     * In case of no connectivity error method returns courses from database,
     * otherwise method returns an empty list
     * */
    private Function<Throwable, ObservableSource<List<Course>>> onErrorCourses(int specialityId, int termId) {
        return throwable -> {
            if (throwable instanceof NoConnectivityException) {
                return subjectsLocalStore.getCourses(specialityId, termId).map(SubjectMapper.FromDB::unmapCourses);
            }
            return Observable.just(new ArrayList<>());
        };
    }

    /**
     * This method saves courses to database
     * */
    private Function<List<Lesson>, List<Lesson>> onSuccessLessons() {
        return lessons -> {

            Observable.just(lessons)
                    .map(lessons1 -> mapLessons((ArrayList<Lesson>) lessons1))
                    .doOnNext(lessonsDbs -> subjectsLocalStore.saveLessons(lessonsDbs))
                    .subscribe();
            return lessons;
        };
    }

    /**
     * In case of no connectivity error method returns lessons from database,
     * otherwise method returns an empty list
     * */
    private Function<Throwable, ObservableSource<List<Lesson>>> onErrorLessons(int specialityId, int lessonTypeId){
        return throwable -> {
            if (throwable instanceof NoConnectivityException){
                return subjectsLocalStore.getLessons(specialityId, lessonTypeId).map(SubjectMapper.FromDB::unmapLessons);
            }
            return Observable.just(new ArrayList<>());
        };
    }
}
