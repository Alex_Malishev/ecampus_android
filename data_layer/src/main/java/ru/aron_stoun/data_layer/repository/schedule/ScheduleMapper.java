package ru.aron_stoun.data_layer.repository.schedule;

import java.util.ArrayList;
import java.util.List;

import ru.aron_stoun.data_layer.model.db.ClassroomDB;
import ru.aron_stoun.data_layer.model.db.ClassroomDBEntity;
import ru.aron_stoun.data_layer.model.db.GroupDB;
import ru.aron_stoun.data_layer.model.db.GroupDBEntity;
import ru.aron_stoun.data_layer.model.db.PairTimeDB;
import ru.aron_stoun.data_layer.model.db.PairTimeDBEntity;
import ru.aron_stoun.data_layer.model.db.ScheduleLessonDB;
import ru.aron_stoun.data_layer.model.db.ScheduleLessonDBEntity;
import ru.aron_stoun.data_layer.model.db.TeacherDB;
import ru.aron_stoun.data_layer.model.db.TeacherDBEntity;
import ru.aron_stoun.data_layer.model.db.WeekDB;
import ru.aron_stoun.data_layer.model.db.WeekDBEntity;
import ru.aron_stoun.data_layer.model.db.WeekdayDB;
import ru.aron_stoun.data_layer.model.db.WeekdayDBEntity;
import ru.aron_stoun.domain_layer.DateUtils;
import ru.aron_stoun.domain_layer.models.Classroom;
import ru.aron_stoun.domain_layer.models.Group;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.models.ScheduleLesson;
import ru.aron_stoun.domain_layer.models.Teacher;
import ru.aron_stoun.domain_layer.models.Week;
import ru.aron_stoun.domain_layer.models.Weekday;

public class ScheduleMapper {

    public static class ToDB {

        public static ArrayList<WeekDBEntity> mapWeeks(ArrayList<Week> weeks){
            ArrayList<WeekDBEntity> weekDBS = new ArrayList<>();
            if (weeks == null) return weekDBS;

            for (Week week : weeks) {
                WeekDBEntity weekDBEntity = new WeekDBEntity();
                weekDBEntity.dateBegin(week.getDateBegin());
                weekDBEntity.dateEnd(week.getDateEnd());
                weekDBEntity.number(week.getNumber());
                weekDBS.add(weekDBEntity);
            }

            return weekDBS;
        }

        public static ArrayList<PairTimeDBEntity> mapPairTimeSchedule(ArrayList<PairTime> pairTimes) {
            ArrayList<PairTimeDBEntity> pairTimeDBEntities = new ArrayList<>();
            if (pairTimes == null) return pairTimeDBEntities;
            int count = 0;
            for (PairTime pairTime: pairTimes) {
                PairTimeDBEntity pairTimeDBEntity = new PairTimeDBEntity();
                pairTimeDBEntity.id(count);
                pairTimeDBEntity.hourStart(pairTime.getHourStart());
                pairTimeDBEntity.hourEnd(pairTime.getHourEnd());
                pairTimeDBEntity.minuteStart(pairTime.getMinuteStart());
                pairTimeDBEntity.minuteEnd(pairTime.getMinuteEnd());
                pairTimeDBEntity.duration(pairTime.getDuration());
                pairTimeDBEntity.startTime(pairTime.getStartTime());
                pairTimeDBEntity.endTime(pairTime.getEndTime());
                pairTimeDBEntity.setActual(pairTime.isCurrent());
                pairTimeDBEntity.name(pairTime.getName());
                pairTimeDBEntities.add(pairTimeDBEntity);
                count++;

            }
            return pairTimeDBEntities;
        }

        public static ArrayList<WeekdayDB> mapWeekday(List<Weekday> weekdays){
            ArrayList<WeekdayDB> weekdayDBS = new ArrayList<>();
            if (weekdays == null) return weekdayDBS;
            for (Weekday weekday : weekdays) {
                WeekdayDBEntity weekdayDBEntity = new WeekdayDBEntity();
                weekdayDBEntity.name(weekday.getName());
                weekdayDBEntity.weekDate(DateUtils.dateFromString(weekday.getDate()));
                weekdayDBEntity.lessons(mapScheduleLesson(weekday.getLessons()));
                weekdayDBS.add(weekdayDBEntity);
            }

            return weekdayDBS;
        }

        public static ArrayList<ScheduleLessonDB> mapScheduleLesson(ArrayList<ScheduleLesson> lessons){
            ArrayList<ScheduleLessonDB> scheduleLessonDBS = new ArrayList<>();
            if (lessons == null) return scheduleLessonDBS;
            for (ScheduleLesson lesson : lessons) {
                ScheduleLessonDBEntity scheduleLessonDBEntity = new ScheduleLessonDBEntity();
                scheduleLessonDBEntity.id(lesson.getId());
                scheduleLessonDBEntity.lessonDayId(lesson.getLessonDayId());
                scheduleLessonDBEntity.discipline(lesson.getDiscipline());
                scheduleLessonDBEntity.timeBegin(lesson.getTimeBegin());
                scheduleLessonDBEntity.timeEnd(lesson.getTimeEnd());
                scheduleLessonDBEntity.classroom(mapClassroom(lesson.getClassroom()));
                scheduleLessonDBEntity.lessonType(lesson.getLessonType());
                scheduleLessonDBEntity.pairNumberStart(lesson.getPairNumberStart());
                scheduleLessonDBEntity.pairNumberEnd(lesson.getPairNumberEnd());
                scheduleLessonDBEntity.teacher(mapTeacher(lesson.getTeacher()));
                scheduleLessonDBEntity.groups(mapGroups(lesson.getGroups()));
                scheduleLessonDBEntity.setActual(lesson.isCurrent());
                scheduleLessonDBEntity.lessonName(lesson.getLessonName());
                scheduleLessonDBS.add(scheduleLessonDBEntity);
            }

            return scheduleLessonDBS;
        }

        public static ClassroomDB mapClassroom(Classroom classroom){
            if (classroom == null) return null;
            ClassroomDBEntity classroomDBEntity = new ClassroomDBEntity();
            classroomDBEntity.id(classroom.getId());
            classroomDBEntity.name(classroom.getName());
            return classroomDBEntity;
        }

        public static TeacherDB mapTeacher(Teacher teacher){
            if (teacher == null) return null;
            TeacherDBEntity teacherDBEntity = new TeacherDBEntity();
            teacherDBEntity.id(teacher.getId());
            teacherDBEntity.name(teacher.getName());
            return teacherDBEntity;
        }

        public static ArrayList<GroupDB> mapGroups(ArrayList<Group> groups){
            ArrayList<GroupDB> groupDBS = new ArrayList<>();
            if (groups == null) return groupDBS;
            for (Group group : groups) {
                GroupDBEntity groupDBEntity = new GroupDBEntity();
                groupDBEntity.id(group.getId());
                groupDBEntity.name(group.getName());
                groupDBEntity.subgroup(group.getSubgroup());
                groupDBS.add(groupDBEntity);
            }
            return groupDBS;
        }
    }


    public static class FromDB {

        public static ArrayList<Week> unmapWeeks(List<WeekDB> weekDBS){
            ArrayList<Week> weeks = new ArrayList<>();
            if (weekDBS == null) return weeks;
            for (WeekDB weekDB : weekDBS) {
                Week week = new Week();
                week.setDateBegin(weekDB.dateBegin());
                week.setDateEnd(weekDB.dateEnd());
                week.setNumber(weekDB.number());
                weeks.add(week);
            }

            return weeks;
        }

        public static List<PairTime> unmapPairTimeSchedule(List<PairTimeDB> pairTimeDBS) {
            ArrayList<PairTime> pairTimes = new ArrayList<>();
            if (pairTimeDBS == null) return pairTimes;

            for (PairTimeDB pairTimeDB: pairTimeDBS) {
                PairTime pairTime = new PairTime();
                pairTime.setHourStart(pairTimeDB.hourStart());
                pairTime.setHourEnd(pairTimeDB.hourEnd());
                pairTime.setMinuteStart(pairTimeDB.minuteStart());
                pairTime.setMinuteEnd(pairTimeDB.minuteEnd());
                pairTime.setDuration(pairTimeDB.duration());
                pairTime.setStartTime(pairTimeDB.startTime());
                pairTime.setEndTime(pairTimeDB.endTime());
                pairTime.setCurrent(pairTimeDB.isActual());
                pairTime.setName(pairTimeDB.name());
                pairTimes.add(pairTime);

            }
            return pairTimes;
        }

        public static ArrayList<Weekday> unmapWeekday(List<WeekdayDB> weekdayDBS){
            ArrayList<Weekday> weekdays = new ArrayList<>();
            if (weekdayDBS == null) return weekdays;
            for (WeekdayDB weekdayDB : weekdayDBS) {
                Weekday weekday = new Weekday();
                weekday.setName(weekdayDB.name());
                weekday.setDate(DateUtils.formatDate(weekdayDB.weekDate()));
                weekday.setLessons(unmapScheduleLessons(weekdayDB.lessons()));
                weekdays.add(weekday);
            }

            return weekdays;
        }

        public static ArrayList<ScheduleLesson> unmapScheduleLessons(List<ScheduleLessonDB> scheduleLessonDBS){
            ArrayList<ScheduleLesson> scheduleLessons = new ArrayList<>();
            if(scheduleLessonDBS == null) return scheduleLessons;
            for (ScheduleLessonDB scheduleLessonDB : scheduleLessonDBS) {
                ScheduleLesson scheduleLesson = new ScheduleLesson();
                scheduleLesson.setId(scheduleLesson.getId());
                scheduleLesson.setLessonDayId(scheduleLessonDB.lessonDayId());
                scheduleLesson.setDiscipline(scheduleLessonDB.discipline());
                scheduleLesson.setTimeBegin(scheduleLessonDB.timeBegin());
                scheduleLesson.setTimeEnd(scheduleLessonDB.timeEnd());
                scheduleLesson.setClassroom(unmapClassroom(scheduleLessonDB.classroom()));
                scheduleLesson.setLessonType(scheduleLessonDB.lessonType());
                scheduleLesson.setPairNumberStart(scheduleLessonDB.pairNumberStart());
                scheduleLesson.setPairNumberEnd(scheduleLessonDB.pairNumberEnd());
                scheduleLesson.setTeacher(unmapTeacher(scheduleLessonDB.teacher()));
                scheduleLesson.setGroups(unmapGroup(scheduleLessonDB.groups()));
                scheduleLesson.setCurrent(scheduleLessonDB.isActual());
                scheduleLesson.setLessonName(scheduleLessonDB.lessonName());
                scheduleLessons.add(scheduleLesson);

            }
            return scheduleLessons;
        }

        public static Teacher unmapTeacher(TeacherDB teacherDB){
            if (teacherDB == null) return null;
            Teacher teacher = new Teacher();
            teacher.setId(teacherDB.id());
            teacher.setName(teacherDB.name());
            return teacher;
        }

        public static Classroom unmapClassroom(ClassroomDB classroomDB){
            if (classroomDB == null) return null;
            Classroom classroom = new Classroom();
            classroom.setId(classroomDB.id());
            classroom.setName(classroomDB.name());
            return classroom;
        }

        public static ArrayList<Group> unmapGroup(List<GroupDB> groupDBS){
            ArrayList<Group> groups = new ArrayList<>();
            if (groupDBS == null) return groups;
            for (GroupDB groupDB : groupDBS) {
                Group group = new Group();
                group.setId(groupDB.id());
                group.setName(groupDB.name());
                group.setSubgroup(groupDB.subgroup());
                groups.add(group);
            }

            return groups;
        }
    }
}
