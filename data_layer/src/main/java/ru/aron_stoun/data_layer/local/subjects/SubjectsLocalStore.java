package ru.aron_stoun.data_layer.local.subjects;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.aron_stoun.data_layer.local.DatabaseHelper;

import ru.aron_stoun.data_layer.model.db.AcademicYearDB;
import ru.aron_stoun.data_layer.model.db.CourseDB;
import ru.aron_stoun.data_layer.model.db.CourseDBEntity;
import ru.aron_stoun.data_layer.model.db.LessonDB;
import ru.aron_stoun.data_layer.model.db.LessonTypeDB;
import ru.aron_stoun.data_layer.model.db.SpecialityDB;
import ru.aron_stoun.data_layer.model.db.SpecialityDBEntity;
import ru.aron_stoun.data_layer.model.db.TermDb;
import ru.aron_stoun.data_layer.model.db.TermDbEntity;

public class SubjectsLocalStore implements ISubjectLocalStore {


    private static final String TAG = SubjectsLocalStore.class.getSimpleName();
    private DatabaseHelper databaseHelper;

    @Inject
    public SubjectsLocalStore(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Observable<List<SpecialityDB>> getSpecialities() {
        return Observable.just(
                (List<SpecialityDB>) databaseHelper.select(SpecialityDBEntity.class).get().toList()
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public Observable<List<CourseDB>> getCourses(int specialityId, int termId) {
        TermDbEntity termDbEntity = new TermDbEntity();
        termDbEntity.id(termId);
        return Observable.just(
                (List<CourseDB>) databaseHelper.select(CourseDB.class).where(CourseDBEntity.TERM.equal(termDbEntity))
                        .orderBy(CourseDBEntity.NAME.lower().asc()).get().toList()
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public Observable<List<LessonDB>> getLessons(int specialityId, int lessonTypeId) {
        return null;
    }

    @Override
    public void saveSpecialities(List<SpecialityDB> specialities) {
        databaseHelper.runTransaction(() -> {
            Observable.fromIterable(specialities)
                    .doOnNext(specialityDB -> databaseHelper.upsert(specialityDB))
                    .subscribe();

            return null;
        });
    }

    @Override
    public void saveCourses(int termId, List<CourseDB> courses) {
        databaseHelper.runTransaction(() -> {
            updateTermCourses(termId, courses);
            return null;
        });
    }

    @Override
    public void saveLessons(List<LessonDB> lessons) {
        databaseHelper.runTransaction(() -> {
            Observable.fromIterable(lessons)
                    .doOnNext(lesson -> databaseHelper.upsert(lesson))
                    .subscribe();
            return null;
        });
    }

    private void saveAcademicYears( List<AcademicYearDB> academicYearDBS) {
        Observable.fromIterable(academicYearDBS)
                .doOnNext(academicYearDB -> {
                    saveTerms(academicYearDB.terms());
                    databaseHelper.upsert(academicYearDB);
                }).subscribe();
    }

    private void saveLessonType(List<LessonTypeDB> lessonTypeDBS) {
        Observable.fromIterable(lessonTypeDBS)
                .doOnNext(lesson -> databaseHelper.upsert(lesson))
                .subscribe();
    }

    private void saveTerms(List<TermDb> termDbs) {
        Observable.fromIterable(termDbs)
                .doOnNext(termDb -> databaseHelper.upsert(termDb))
                .subscribe();
    }


    /**
     * This method selects {@link TermDb} from database and updates its courses array
     * @param termId
     * @param courses
     */
    private void updateTermCourses(int termId, List<CourseDB> courses){
        Observable.just((TermDb)databaseHelper.select(TermDb.class).where(TermDbEntity.ID.eq(termId)).get().firstOrNull())
                .map(termDb -> {
                    TermDbEntity termDbEntity = new TermDbEntity();
                    termDbEntity.id(termDb.id());
                    termDbEntity.parentId(termDb.parentId());
                    termDbEntity.name(termDb.name());
                    termDbEntity.setActual(termDb.isActual());
                    termDbEntity.courses(courses);
                    termDbEntity.termTypeName(termDb.termTypeName());
                    databaseHelper.upsert(termDbEntity);
                    return termDb;
                })
                .subscribe();

    }

}
