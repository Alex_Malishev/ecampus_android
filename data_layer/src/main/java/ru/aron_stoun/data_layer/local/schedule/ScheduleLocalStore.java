package ru.aron_stoun.data_layer.local.schedule;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.aron_stoun.data_layer.local.DatabaseHelper;
import ru.aron_stoun.data_layer.model.db.GroupDB;
import ru.aron_stoun.data_layer.model.db.PairTimeDB;
import ru.aron_stoun.data_layer.model.db.PairTimeDBEntity;
import ru.aron_stoun.data_layer.model.db.ScheduleLessonDB;
import ru.aron_stoun.data_layer.model.db.WeekDB;
import ru.aron_stoun.data_layer.model.db.WeekDBEntity;
import ru.aron_stoun.data_layer.model.db.WeekdayDB;
import ru.aron_stoun.data_layer.model.db.WeekdayDBEntity;
import ru.aron_stoun.data_layer.utils.LogUtils;
import ru.aron_stoun.domain_layer.models.Week;


@SuppressWarnings("unchecked")
public class ScheduleLocalStore implements IScheduleLocalStore {


    private static final String TAG = ScheduleLocalStore.class.getSimpleName();
    private DatabaseHelper databaseHelper;

    @Inject
    public ScheduleLocalStore(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    /**
     * Method retrieve list of days with schedule.
     * @param dateStart {@link Date} date of first day in a schedule of week
     * @return {@link  Observable<List>} of {@link WeekdayDB}
     */
    @Override
    public Observable<List<WeekdayDB>> getScheduleWeek(Date dateStart) {
        Date dateEnd = dateStart;
        Calendar c = Calendar.getInstance();
        c.setTime(dateEnd);
        c.add(Calendar.DATE, 7);
        dateEnd = c.getTime();
        return Observable.just(
                (List<WeekdayDB>) databaseHelper.select(WeekdayDBEntity.class)
                        .where(WeekdayDBEntity.WEEK_DATE.greaterThanOrEqual(dateStart).and(WeekdayDBEntity.WEEK_DATE.lessThan(dateEnd)))
                        .get()
                        .toList()
        );
    }

    /**
     * Methods saves week schedule in database
     * @param weekdayDBS {@link List<WeekdayDB>}
     */
    @Override
    public void saveScheduleWeek(List<WeekdayDB> weekdayDBS) {
        databaseHelper.runTransaction(() -> {
//            databaseHelper.delete(WeekdayDB.class);
            Observable.fromIterable(weekdayDBS)
                    .doOnNext(weekdayDB -> {
                        for (ScheduleLessonDB scheduleLessonDB : weekdayDB.lessons()) {
                            databaseHelper.upsert(scheduleLessonDB.classroom());
                            databaseHelper.upsert(scheduleLessonDB.teacher());
                            for (GroupDB g : scheduleLessonDB.groups()) {
                                databaseHelper.upsert(g);
                            }
                        }

                        databaseHelper.deleteWithCondition(WeekdayDB.class).where(WeekdayDBEntity.WEEK_DATE.eq(weekdayDB.weekDate())).get().value();
                        databaseHelper.upsert(weekdayDB);
                    })
                    .doOnError(throwable -> LogUtils.e(TAG, "error while save weekday - " + throwable.getMessage()))
                    .subscribe();
            return null;
        });
    }

    /**
     * This method save information about weeks in database.
     * If current week is even, it will be saved first, otherwise it will be saved last
     * @param weeks {@link ArrayList<WeekdayDBEntity>} list of weeks
     * @param currentIndex integer index of current week in list
     */
    @Override
    public void saveWeeks(ArrayList<WeekDBEntity> weeks, int currentIndex) {
        ArrayList<WeekDBEntity> weeksToSave = new ArrayList<>();
        WeekDBEntity current = weeks.get(currentIndex);
        current.setActual(true);
        boolean isCurrentWeekFirst = current.number() == Week.FIRST;
        WeekDBEntity neighborWeek = isCurrentWeekFirst ?
                weeks.get(currentIndex + 1) : weeks.get(currentIndex - 1);
        neighborWeek.setActual(false);
        if (isCurrentWeekFirst){
            weeksToSave.add(current);
            weeksToSave.add(neighborWeek);
        }else {
            weeksToSave.add(neighborWeek);
            weeksToSave.add(current);
        }
        databaseHelper.runTransaction(()->{
            Observable.fromIterable(weeksToSave)
                    .doOnNext(week -> databaseHelper.upsert(week))
                    .subscribe();
            return null;
        });
    }

    /**
     * Retrieves list of all saved weeks with information from database
     * @return {@link Observable<List>} with {@link WeekDB}
     */
    @Override
    public Observable<List<WeekDB>> getWeeks() {
        return Observable.just(
                (List<WeekDB>)databaseHelper.select(WeekDBEntity.class).get().toList()
        );
    }

    /**
     * Method saves pair time schedule
     * @param pairTimeDBS {@link ArrayList<PairTimeDBEntity>} list of DB implementation of {@link PairTimeDB}
     */
    @Override
    public void savePairTimeSchedule(ArrayList<PairTimeDBEntity> pairTimeDBS) {
        databaseHelper.runTransaction(() -> {
            databaseHelper.delete(PairTimeDB.class);
            Observable.fromIterable(pairTimeDBS)
                    .doOnNext(pairTimeDBEntity -> databaseHelper.upsert(pairTimeDBEntity))
                    .subscribe();
            return null;
        });
    }


    /**
     * Retrieves pairtime schedule from database
     * @return {@link Observable<List>} with {@link PairTimeDB}
     */
    @Override
    public Observable<List<PairTimeDB>> getPairTimeSchedule() {
        return Observable.just(
                (List<PairTimeDB>) databaseHelper.select(PairTimeDBEntity.class).get().toList()
        );
    }
}
