package ru.aron_stoun.data_layer.local.auth;

import javax.inject.Inject;

import ru.aron_stoun.data_layer.local.DatabaseHelper;
import ru.aron_stoun.data_layer.model.db.Credentials;
import ru.aron_stoun.data_layer.model.db.CredentialsEntity;


public class AuthLocalStore implements IAuthLocalStore {

    private final DatabaseHelper databaseHelper;

    @Inject
    public AuthLocalStore(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    /**
     * Method saves user's login and password
     * @param login {@link String}
     * @param password {@link String}
     */
    @Override
    public void saveCredentials(String login, String password) {
        CredentialsEntity credentialsEntity = new CredentialsEntity();
        credentialsEntity.login(login);
        credentialsEntity.password(password);
        databaseHelper.save(credentialsEntity);
    }

    /**
     * Method retrieves user's credentials
     * @return {@link Credentials}
     */
    @Override
    public Credentials getCredentials() {
        return (Credentials) databaseHelper.select(Credentials.class, null);
    }
}
