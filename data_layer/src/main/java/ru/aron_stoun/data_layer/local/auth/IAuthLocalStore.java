package ru.aron_stoun.data_layer.local.auth;


import ru.aron_stoun.data_layer.model.db.Credentials;

public interface IAuthLocalStore {

    void saveCredentials(String login, String password);

    Credentials getCredentials();
}
