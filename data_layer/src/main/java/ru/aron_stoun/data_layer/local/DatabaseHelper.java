package ru.aron_stoun.data_layer.local;

import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.requery.Persistable;
import io.requery.query.Condition;
import io.requery.query.Deletion;
import io.requery.query.Result;
import io.requery.query.Scalar;
import io.requery.query.Selection;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.sql.EntityDataStore;


/**
 * Created by jarvi on 03.08.2017.
 */
@Singleton
public class DatabaseHelper {

    private static final String TAG = "DatabaseHelper";
    private final EntityDataStore<Persistable> mDataStore;
    private ReactiveEntityStore<Persistable> mRxDataStore;

    @Inject
    public DatabaseHelper(ReactiveEntityStore<Persistable> mDataStore, EntityDataStore<Persistable> entityDataStore) {
        this.mRxDataStore = mDataStore;
        this.mDataStore = entityDataStore;
    }

    public void save(Persistable entityModel){
        mDataStore.delete(entityModel.getClass()).get().value();
        mDataStore.insert(entityModel);
    }

    public void upsert(Persistable persistable){
        if (persistable != null)mDataStore.upsert(persistable);
    }



    public Persistable select(Class<? extends Persistable> entityClass, Condition condition){
        return (Persistable) mDataStore.select(entityClass)
                .get().firstOrNull();
    }

    public Selection<? extends Result<? extends Persistable>> select(Class<? extends Persistable> entityClass){
//        refresh(entityClass);
        return mDataStore.select(entityClass);
    }

    public void runTransaction(Callable<? extends Persistable> callable){
        mDataStore.runInTransaction(callable);
    }

    public Persistable refresh(Persistable persistable){
        return mDataStore.refreshAll(persistable);
    }


    public void delete(Class<? extends Persistable> type) {
        mDataStore.delete(type).get().value();
    }

    public Deletion<? extends Scalar<Integer>> deleteWithCondition(Class<? extends Persistable> type) {
        return mDataStore.delete(type);
    }

    public Selection<? extends Scalar<Integer>> count(Class<? extends Persistable> type) {
        return mDataStore.count(type);
    }
}
