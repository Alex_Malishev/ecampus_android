package ru.aron_stoun.data_layer.local.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import ru.aron_stoun.data_layer.model.db.PairTimeDB;
import ru.aron_stoun.data_layer.model.db.PairTimeDBEntity;
import ru.aron_stoun.data_layer.model.db.WeekDB;
import ru.aron_stoun.data_layer.model.db.WeekDBEntity;
import ru.aron_stoun.data_layer.model.db.WeekdayDB;


public interface IScheduleLocalStore {

    Observable<List<WeekdayDB>> getScheduleWeek(Date dateStart);
    void saveScheduleWeek(List<WeekdayDB> weekdayDBS);

    void saveWeeks(ArrayList<WeekDBEntity> weeks, int currentIndex);
    Observable<List<WeekDB>> getWeeks();

    void savePairTimeSchedule(ArrayList<PairTimeDBEntity> pairTimeDBS);
    Observable<List<PairTimeDB>> getPairTimeSchedule();
}
