package ru.aron_stoun.data_layer.local.subjects;

import java.util.List;

import io.reactivex.Observable;
import ru.aron_stoun.data_layer.model.db.CourseDB;
import ru.aron_stoun.data_layer.model.db.LessonDB;
import ru.aron_stoun.data_layer.model.db.SpecialityDB;


public interface ISubjectLocalStore {

    Observable<List<SpecialityDB>> getSpecialities();
    Observable<List<CourseDB>> getCourses(int specialityId, int termId);
    Observable<List<LessonDB>> getLessons(int specialityId, int lessonTypeId);
    void saveSpecialities(List<SpecialityDB> specialities);
    void saveCourses(int termId, List<CourseDB> courses);
    void saveLessons(List<LessonDB> lessons);
}
