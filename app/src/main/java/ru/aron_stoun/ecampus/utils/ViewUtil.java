package ru.aron_stoun.ecampus.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Field;

import ru.aron_stoun.ecampus.EcampusApp;
import ru.aron_stoun.ecampus.R;


public final class ViewUtil {

    private static final String TAG = ViewUtil.class.getSimpleName();

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static int getScreenWidth() {
        return (int) pxToDp(Resources.getSystem().getDisplayMetrics().widthPixels);
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static int getActionBarHeight(Activity activity) {
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static Snackbar makeConnectionSnackbar(View containerLayout, int stringId, boolean isCentered) {
        Snackbar snackbar = Snackbar.make(containerLayout, "", Snackbar.LENGTH_INDEFINITE);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        layout.setBackgroundColor(ContextCompat.getColor(EcampusApp.get(), android.R.color.holo_red_light));
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

        layout.getLayoutParams().height = ViewUtil.dpToPx(25);

        View snackView = LayoutInflater.from(EcampusApp.get()).inflate(R.layout.my_snackbar, null);
        TextView textViewTop = (TextView) snackView.findViewById(R.id.text);
        textViewTop.setText(stringId);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textViewTop.getLayoutParams();
        if (isCentered){
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        }else {
            layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

        }
        textViewTop.setTextColor(Color.WHITE);

        layout.setPadding(0, 0, 0, 0);

        layout.addView(snackView, 0);
        return snackbar;
    }


    public static boolean isTextIsNumeric(String text) {
        Log.e(TAG, "isTextIsNumeric: " + text);
        String newText = text.replaceAll("\\.", "");
        Log.e(TAG, "isTextIsNumeric: new text " + newText);
        char[] textChar = newText.toCharArray();
        for (int i = 0; i < newText.length(); i++) {
            Log.e(TAG, "isTextIsNumeric: char is " + textChar[i]);
            switch (textChar[i]) {
                case 'Д':
                    return false;
                case 'М':
                    return false;
                case 'Г':
                    return false;
            }
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    public static void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShifting(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }


}
