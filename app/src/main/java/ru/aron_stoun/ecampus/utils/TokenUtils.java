package ru.aron_stoun.ecampus.utils;

import android.content.SharedPreferences;

import ru.aron_stoun.ecampus.EcampusApp;


/**
 * Created by jarvi on 15.08.2017.
 */

public class TokenUtils {

    private static final String TOKEN_STORAGE = "token_storage";
    private static final String TOKEN = "token";
    private static String userPhone;
    public static boolean isProfileExist = false;

    public static String getUserPhone() {
        return userPhone;
    }

    public static void setUserPhone(String userPhone) {
        TokenUtils.userPhone = userPhone;
    }

    public static String getToken(){
        SharedPreferences sPref = EcampusApp.get().getSharedPreferences(TOKEN_STORAGE, EcampusApp.MODE_PRIVATE);
        return sPref.getString(TOKEN, "");
    }

    public static void clearToken(){
        SharedPreferences sPref = EcampusApp.get().getSharedPreferences(TOKEN_STORAGE, EcampusApp.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.clear();
        ed.apply();
    }

    public static void saveToken(String token){
        SharedPreferences sPref = EcampusApp.get().getSharedPreferences(TOKEN_STORAGE, EcampusApp.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.clear();
        ed.putString(TOKEN, token);
        ed.apply();
    }
}
