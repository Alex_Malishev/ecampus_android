package ru.aron_stoun.ecampus.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by jarvi on 16.08.2017.
 */

public class DateUtils {

    public static SimpleDateFormat getDateFormat(){
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
    }

    public static SimpleDateFormat getDateTimeFormat(){
        return  new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
    }

    public static SimpleDateFormat getVKDateFormat(){
        return  new SimpleDateFormat("d.M.yyyy", Locale.getDefault());
    }
    public static SimpleDateFormat getFBDateFormat(){
        return  new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    }

    public static SimpleDateFormat getInverseDateFormat(){
        return  new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public static SimpleDateFormat getPrettyDateFormat(){
        return  new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    }


    public static String formatDateWithWord(String date){
        try {
            return new SimpleDateFormat("dd MMMM", Locale.getDefault()).format(getDateFormat().parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatDate(Date date){
        return getDateFormat().format(date);
    }

    public static String formatDate(String date){
        if (date == null || date.isEmpty()) return "";
        return getInverseDateFormat().format(dateFromStringInverse(date));
    }

    public static String formatVKDate(String date){
        if (date == null || date.isEmpty()) return "";
        try {
            return getDateFormat().format(getVKDateFormat().parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatFBDate(String date){
        if (date == null || date.isEmpty()) return "";
        try {
            return getDateFormat().format(getFBDateFormat().parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String formatDateString(String date){
        return getDateFormat().format(dateFromString(date));
    }

    public static String formatDateTimeString(String date){
        return getDateTimeFormat().format(dateFromString(date));
    }

    public static Date dateFromString(String date){
        try {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String prettyDateStringFromString(String date){
        return getPrettyDateFormat().format(dateFromString(date));
    }

    public static String normalDateStringFromInverseString(String date){
        if (date == null || date.isEmpty()) return "";
        try {
            return getDateFormat().format(getInverseDateFormat().parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Date dateFromStringInverse(String date){
        try {
            return new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
            return null;
        }
    }

    public static int daysBetweenDateAndToday(String date){
        Calendar testCalendar = Calendar.getInstance();
        testCalendar.setTime(dateFromString(date));
        long msDiff = testCalendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis() ;
        long daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff);
        return (int) daysDiff;
    }

}
