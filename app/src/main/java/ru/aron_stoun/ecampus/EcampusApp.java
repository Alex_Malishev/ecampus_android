package ru.aron_stoun.ecampus;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import ru.aron_stoun.data_layer.DataLayer;
import ru.aron_stoun.ecampus.injection.component.ApplicationComponent;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;
import ru.aron_stoun.ecampus.injection.component.DaggerApplicationComponent;
import ru.aron_stoun.ecampus.injection.component.DaggerCustomViewComponent;

public class EcampusApp extends DaggerApplication {

    public static EcampusApp sInstance;
    private CustomViewComponent customViewComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {

        return DaggerApplicationComponent.builder()
                .application(this)
                .dataLayer(DataLayer.init(this))
                .customView(getCustomViewComponent())
                .build();
    }


    public static EcampusApp get() {
        return (EcampusApp) sInstance.getApplicationContext();
    }

    public static EcampusApp getApplication(){
        return sInstance;
    }


    public CustomViewComponent getCustomViewComponent() {

        if (customViewComponent == null) {

            customViewComponent = DaggerCustomViewComponent.builder().build();

        }
        return customViewComponent;
    }



}
