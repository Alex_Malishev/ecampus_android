package ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseView;
import ru.aron_stoun.ecampus.presentation_layer.base.Presenter;

public interface PairTimeScheduleContract {

    interface PairTimePresenter{
        void getPairTimeSchedule();
    }

    interface PairTimeScheduleMvpView extends BaseView{
        void onPairTimeSchedule(ArrayList<PairTime> pairTimes);
    }
}
