package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleInteractor;
import ru.aron_stoun.domain_layer.usecase.my_schedule.ScheduleInteractor;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.injection.FragmentScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;

@SuppressLint("CheckResult")
@ActivityScope
@InjectViewState
public class MySchedulePresenter extends BasePresenter<MyScheduleContract.MyScheduleMvpView>
        implements MyScheduleContract.MySchedulePresenter {


    private IScheduleInteractor scheduleInteractor;
    private int targetType;
    private int id;

    @Inject
    public MySchedulePresenter(IScheduleInteractor scheduleInteractor) {
        this.scheduleInteractor = scheduleInteractor;
    }

    @Override
    public void getMyScheduleWeeks() {
        addDisposable(scheduleInteractor.getMyScheduleWeeks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        myScheduleWeeks -> {
                            id = myScheduleWeeks.getModel().getId();
                            targetType = myScheduleWeeks.getModel().getType();
                            getViewState().onMyScheduleWeeks(myScheduleWeeks.getWeeks(), myScheduleWeeks.getCurrentWeekIndex());
                        },
                        throwable -> {},
                        ()-> getViewState().onProgressStop(),
                        disposable -> {
                            getViewState().onProgressStart();
                        }
                ));
    }

    @Override
    public void getSchedule(String date) {
        addDisposable(scheduleInteractor.getMySchedule(date, id, targetType)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        weekdays -> getViewState().onSchedule((ArrayList<Weekday>) weekdays),
                        throwable -> {},
                        ()-> getViewState().onProgressStop(),
                        disposable -> getViewState().onProgressStart()
                ));
    }
}
