package ru.aron_stoun.ecampus.presentation_layer.ui.subjects;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import javax.inject.Inject;

import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.subject.SubjectView;

public class SubjectsRecyclerAdapter extends BaseRecyclerAdapter<SubjectsRecyclerAdapter.SubjectHolder> {

    @Inject
    public SubjectsRecyclerAdapter() {
        setUpdatable(false);
    }

    @NonNull
    @Override
    public SubjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SubjectView itemView = new SubjectView(parent.getContext());
        return new SubjectHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectHolder holder, int position) {
        Course course = (Course) mCollection.get(position);
        holder.setCourse(course);
        super.onBindViewHolder(holder, position);
    }

    class SubjectHolder extends RecyclerView.ViewHolder{

        SubjectHolder(@NonNull SubjectView itemView) {
            super(itemView);
        }

        void setCourse(Course course){
            ((SubjectView) itemView).setCourse(course);
        }
    }
}
