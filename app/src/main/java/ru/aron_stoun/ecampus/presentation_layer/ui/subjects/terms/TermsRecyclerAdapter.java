package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.terms;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;
import ru.aron_stoun.domain_layer.models.Term;

public class TermsRecyclerAdapter extends BaseRecyclerAdapter<TermsRecyclerAdapter.TermHolder> {


    private int activeUserPosition = -1;

    @Inject
    public TermsRecyclerAdapter() {
        setUpdatable(false);
    }

    @NonNull
    @Override
    public TermHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.term_item, parent, false);
        return new TermHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TermHolder holder, int position) {
        Term term = (Term) mCollection.get(position);
        holder.setTerm(term);
//        if (term.isCurrent()){
//            holder.highlightCurrentTerm();
//            if (activeUserPosition == -1){
//                holder.highlight();
//            }
//        }
        if (position == activeUserPosition){
            holder.highlight();
        }else {
            holder.unhighlight();
            if (term.isCurrent()) {
                holder.highlightCurrentTerm();
            }
        }
        super.onBindViewHolder(holder, position);
    }

    @Override
    public void onItemClick(@NonNull TermHolder holder) {
        activeUserPosition = holder.getAdapterPosition();
        super.onItemClick(holder);
    }

    public void removeActiveUserPosition(){
        activeUserPosition = -1;
    }

    class TermHolder extends RecyclerView.ViewHolder{

        TermHolder(@NonNull View itemView) {
            super(itemView);
        }

        void setTerm(Term term){
            ((TextView) itemView).setText(term.getName().concat(" ").concat(term.getTermTypeName()));
        }

        void highlight(){
             itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorTermSelection));
        }

        void highlightCurrentTerm(){
             itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorTermCurrent));
        }

        void unhighlight(){
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.transparent));
        }
    }
}
