package ru.aron_stoun.ecampus.presentation_layer.ui.auth;

import ru.aron_stoun.ecampus.presentation_layer.base.BaseView;
import ru.aron_stoun.ecampus.presentation_layer.base.Presenter;

public interface SplashscreenContract {

    interface SplashPresenter {
        void tryToLogin();
    }

    interface SplashsscreenView extends BaseView{
        void onSuccess();
        void onDenied();
    }
}
