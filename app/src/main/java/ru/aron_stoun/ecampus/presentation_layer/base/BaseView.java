package ru.aron_stoun.ecampus.presentation_layer.base;

import com.arellomobile.mvp.MvpView;

/**
 * Created by jarvi on 01.06.2017.
 */

public interface BaseView extends MvpView {

    void onProgressStart();
    void onProgressStop();
    void onNetworkConnectionLost();
    void onTokenIsDead();
}
