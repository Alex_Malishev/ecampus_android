package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;

public class ScheduleRecyclerAdapter extends BaseRecyclerAdapter<ScheduleRecyclerAdapter.WeekdayHolder> {


    @Inject
    public ScheduleRecyclerAdapter() {
        setUpdatable(false);
    }

    @NonNull
    @Override
    public WeekdayHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        WeekdayView weekdayView = new WeekdayView(parent.getContext());
        return new WeekdayHolder(weekdayView);
    }

    @Override
    public void onBindViewHolder(@NonNull WeekdayHolder holder, int position) {
        Weekday weekday = (Weekday) mCollection.get(position);
        holder.setWeekday(weekday);
        super.onBindViewHolder(holder, position);
    }

    class WeekdayHolder extends RecyclerView.ViewHolder{

        WeekdayHolder(@NonNull View itemView) {
            super(itemView);
        }

        void setWeekday(Weekday weekday){
            ((WeekdayView) itemView).setWeekday(weekday);
        }
    }
}
