package ru.aron_stoun.ecampus.presentation_layer.ui.auth.login;

import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseView;
import ru.aron_stoun.ecampus.presentation_layer.base.Presenter;

public interface LoginContract {

    interface LoginPresenter{
        void login(String username, String password);
    }

    interface LoginMvpView extends BaseView{
        void onLoginSuccess();
        void onFieldsIsValid();
        void onUsernameEmpty();
        void onPasswordEmpty();
        void onError();
    }
}
