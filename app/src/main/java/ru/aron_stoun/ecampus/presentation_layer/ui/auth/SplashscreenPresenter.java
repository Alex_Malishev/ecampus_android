package ru.aron_stoun.ecampus.presentation_layer.ui.auth;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthInteractor;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;

@InjectViewState
@ActivityScope
public class SplashscreenPresenter extends BasePresenter<SplashscreenContract.SplashsscreenView>
        implements SplashscreenContract.SplashPresenter{

    private IAuthInteractor authInteractor;

    @Inject
    public SplashscreenPresenter(IAuthInteractor authInteractor) {
        this.authInteractor = authInteractor;
    }

    @SuppressLint("CheckResult")
    @Override
    public void tryToLogin() {
        addDisposable(authInteractor.tryToLoginIfCredentialExist()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(this::onLoginShotResult, this::onError));
    }

    private void onLoginShotResult(Boolean result){
            if (result){
                getViewState().onSuccess();
            }else {
                getViewState().onDenied();
            }
    }

    private void onError(Throwable throwable){
            getViewState().onDenied();
    }

}
