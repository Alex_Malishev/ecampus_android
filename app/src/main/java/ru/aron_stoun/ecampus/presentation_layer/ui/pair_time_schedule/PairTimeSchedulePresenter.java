package ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleInteractor;
import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;

@SuppressLint("CheckResult")
@InjectViewState
public class PairTimeSchedulePresenter extends BasePresenter<PairTimeScheduleContract.PairTimeScheduleMvpView>
        implements PairTimeScheduleContract.PairTimePresenter{


    private IScheduleInteractor scheduleInteractor;

    @Inject
    public PairTimeSchedulePresenter(IScheduleInteractor scheduleInteractor) {
        this.scheduleInteractor = scheduleInteractor;
    }

    @Override
    public void getPairTimeSchedule() {
        addDisposable(scheduleInteractor.getPairTimeSchedule()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        pairTimes -> getViewState().onPairTimeSchedule((ArrayList<PairTime>) pairTimes),
                        throwable -> {},
                        ()-> getViewState().onProgressStop(),
                        disposable -> getViewState().onProgressStart()
                ));
    }
}
