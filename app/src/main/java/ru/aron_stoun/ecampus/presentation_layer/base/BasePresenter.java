package ru.aron_stoun.ecampus.presentation_layer.base;

import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by jarvi on 01.07.2017.
 */

public class BasePresenter<T extends BaseView> extends MvpPresenter<T>{

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected final void addDisposable(Disposable disposable){
        compositeDisposable.add(disposable);
    }

    protected final void clearDisposable(){
        compositeDisposable.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearDisposable();
    }
}
