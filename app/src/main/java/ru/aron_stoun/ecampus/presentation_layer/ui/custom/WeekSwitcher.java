package ru.aron_stoun.ecampus.presentation_layer.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.domain_layer.models.Week;
import ru.aron_stoun.ecampus.utils.DateUtils;

public class WeekSwitcher extends RelativeLayout {

    public interface WeekSelectionListener{
        void onWeekClicked(Week week);
    }

    private WeekSelectionListener weekSelectionListener;

    private View viewContainer;
    private ArrayList<Week> weeks;
    private HashMap<Integer, ArrayList<Week>> weeksPairs;
    private int currentWeekPosition;
    private int activeUserPosition;

    @BindView(R.id.week_1)
    TextView firstWeekTextView;
    @BindView(R.id.week_2)
    TextView secondWeekTextView;

    @BindView(R.id.next_week_pair)
    ImageButton nextWeekPairButton;

    @BindView(R.id.prev_week_pair)
    ImageButton prevWeekPairButton;

    public WeekSwitcher(Context context) {
        super(context);
        init();
    }

    public WeekSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WeekSwitcher(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setWeekSelectionListener(WeekSelectionListener weekSelectionListener) {
        this.weekSelectionListener = weekSelectionListener;
    }

    private void init() {
        weeks = new ArrayList<>();
        weeksPairs = new HashMap<>();
        viewContainer = inflate(getContext(), R.layout.weeks_switcher, this);
        ButterKnife.bind(this, viewContainer);
    }

    public void setWeeks(int currentWeekPosition, ArrayList<Week> weeks) {
        this.weeks = weeks;
        this.currentWeekPosition = currentWeekPosition;
        this.activeUserPosition = calculateActivePosition(currentWeekPosition);
        apartWeeks();
        updateWeeks(currentWeekPosition);

    }


    /**
     * This method calculates active position assuming weeks are separated by blocks of two
     * @param current Current index of week in weeks array
     * @return active position
     */
    private int calculateActivePosition(int current){
        int step = 0;
        while (step + 2 <= current){
            step++;
        }
        return step;
    }

    private void apartWeeks(){
        int count = 0;
        ArrayList<Week> tempList = new ArrayList<>();
        for (int i = 0; i < weeks.size(); i++) {
            tempList.add(weeks.get(i));
            if (i != 0 && (i + 1) % 2 == 0){
                weeksPairs.put(count, tempList);
                count++;
                tempList.clear();
            }

        }
    }
    private void updateWeeks(int activePosition) {
        Week currentWeek = weeks.get(activePosition);
        weekSelectionListener.onWeekClicked(currentWeek);
        boolean isCurrentWeekFirst = currentWeek.getNumber() == Week.FIRST;
        Week neighborWeek = isCurrentWeekFirst ?
                weeks.get(activePosition + 1) : weeks.get(activePosition - 1);

        String firstWeekText;
        String secondWeekText;
        String date1 = DateUtils.prettyDateStringFromString(currentWeek.getDateBegin()) + " - " +  DateUtils.prettyDateStringFromString(currentWeek.getDateEnd());
        String date2 = DateUtils.prettyDateStringFromString(neighborWeek.getDateBegin()) + " - " +  DateUtils.prettyDateStringFromString(neighborWeek.getDateEnd());
        if (isCurrentWeekFirst) {

            firstWeekText = currentWeek.getNumber() + " " +  getContext().getString(R.string.week);
            secondWeekText = neighborWeek.getNumber() + " " + getContext().getString(R.string.week);

            firstWeekText +=  "\n" + " " + date1;
            secondWeekText +=  "\n" + " " + date2;
        } else {
            secondWeekText = currentWeek.getNumber() + " " + getContext().getString(R.string.week);
            firstWeekText = neighborWeek.getNumber() + " " + getContext().getString(R.string.week);
            firstWeekText +=  "\n" + " " + date2;
            secondWeekText +=  "\n" + " " + date1;
        }


        firstWeekTextView.setText(firstWeekText);
        secondWeekTextView.setText(secondWeekText);
    }

    @OnClick(R.id.next_week_pair)
    public void onNextWeeksPair() {

        activeUserPosition++;
        prevWeekPairButton.setEnabled(true);
        if (activeUserPosition * 2 >= weeks.size() - 1){
            nextWeekPairButton.setEnabled(false);
            activeUserPosition--;
            return;
        }
        int activePosition = activeUserPosition * 2;


        updateWeeks(activePosition);

    }

    @OnClick(R.id.prev_week_pair)
    public void onPrevWeeksPair() {

        activeUserPosition--;

        nextWeekPairButton.setEnabled(true);
        if (activeUserPosition * 2 < 0) {
            activeUserPosition++;
            prevWeekPairButton.setEnabled(false);
            return;
        }

        int activePosition = activeUserPosition * 2;

        updateWeeks(activePosition);
    }

    @OnClick({R.id.week_1, R.id.week_2})
    public void onWeekClick(View view){
        switch (view.getId()){
            case R.id.week_1:
                weekSelectionListener.onWeekClicked(weeks.get(activeUserPosition * 2));
                break;
            case R.id.week_2:
                weekSelectionListener.onWeekClicked(weeks.get(activeUserPosition * 2 + 1));
                break;
        }
    }


}
