package ru.aron_stoun.ecampus.presentation_layer.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.utils.LogUtils;
import ru.aron_stoun.ecampus.utils.ViewUtil;

public abstract class BaseEditText extends FrameLayout implements TextWatcher, View.OnClickListener {

    public interface TextChangedListener{
        void onAfterTextChanged();
    }


    public static final String EMPTY = "";
    public static final String PHONE_START = "+7";

    private static final String TAG = BaseEditText.class.getSimpleName();
    TextInputLayout inputLayout;
    EditText mTextField;
    ImageView mClearButton;
    View mUnderlineStatus;
    private TextChangedListener mTextChangedListener;
    private boolean isValidationEnabled = true;
    private int maxLength;
    private int inputType;
    private int imeOptions;
    private String hint;


    public BaseEditText(@NonNull Context context) {
        super(context);
        setView(initView(context));
    }

    public BaseEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View v = initView(context);
        parseAtribute(context, attrs);
        setView(v);
    }

    public BaseEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View v = initView(context);
        parseAtribute(context, attrs);
        setView(v);

    }

    public void setTextChangedListener(TextChangedListener mTextChangedListener) {
        this.mTextChangedListener = mTextChangedListener;
    }

    public void setValidationEnabled(boolean validationEnabled) {
        isValidationEnabled = validationEnabled;
    }

    public void setMaxLength(int maxLength) {
        if (maxLength == 0) return;
        this.maxLength = maxLength;
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        mTextField.setFilters(fArray);
    }

    public void setText(String text){
        mTextField.setText(text);
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
        mTextField.setInputType(this.inputType);
    }

    public void setImeOptions(int imeOptions) {
        this.imeOptions = imeOptions;
        mTextField.setImeOptions(imeOptions);
    }

    public void setHint(String hint) {
        this.hint = hint;
        inputLayout.setHint(hint);
    }

    public String getText(){
        return mTextField.getText().toString();
    }

    public boolean isEmpty(){
        return getText().isEmpty();
    }

    abstract View initView(Context context);
    abstract void underlineCorrect();
    abstract void underlineError();

    private void parseAtribute(Context context, @Nullable AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseEditText, 0, 0);
        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.BaseEditText_android_hint:
                    setHint(a.getString(attr));
                    break;
                case R.styleable.BaseEditText_android_maxLength:
                    setMaxLength(a.getInt(attr, 0));
                    break;
                //note that you are accessing standart attributes using your attrs identifier
                case R.styleable.BaseEditText_android_inputType:
                    setInputType(a.getInt(attr, EditorInfo.TYPE_TEXT_VARIATION_NORMAL));
                    break;
                case R.styleable.BaseEditText_android_imeOptions:
                    setImeOptions(a.getInt(attr, 0));
                    break;
                default:
                    LogUtils.e(TAG, "Unknown attribute for " + getClass().toString() + ": " + attr);
                    break;
            }
        }
        a.recycle();
    }


    private void setView(View container){
        mClearButton.setOnClickListener(this);
        mTextField.addTextChangedListener(this);
        if (imeOptions != 0) mTextField.setImeOptions(imeOptions);
        mTextField.setInputType(inputType);
        setMaxLength(maxLength);
        inputLayout.setHint(hint);
        this.removeAllViews();
        this.addView(container);
        mTextField.setPadding(mTextField.getPaddingLeft(), mTextField.getPaddingTop(), ViewUtil.dpToPx(45), mTextField.getPaddingBottom());
    }


    private void clearField(){
        if (inputType == InputType.TYPE_CLASS_PHONE){
            mTextField.clearFocus();
            this.requestFocus();
        }
        mTextField.setText(EMPTY);
        hideUnderline();
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (mTextField.isFocused() && isValidationEnabled)validateField();
        mClearButton.setVisibility(s.length() != 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mTextChangedListener != null) mTextChangedListener.onAfterTextChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.field:
                mTextField.requestFocus();
                break;
            case R.id.cross_clear:
                clearField();
                break;
        }
    }


    private void validateField(){
        if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD || inputType == 129){
            LogUtils.e(TAG, "input type password, text length" + getText().length());
            if (getText().length() < 6){
                underlineError();
            }else {
                underlineCorrect();
            }
        }else if (inputType == InputType.TYPE_CLASS_NUMBER){
            if (getText().length() != 6){
                underlineError();
            }else {
                underlineCorrect();
            }
        }else if (inputType == InputType.TYPE_TEXT_VARIATION_PERSON_NAME){
            if (!isEmpty())
                underlineCorrect();
            else
                underlineError();
        }else {
            if (!isEmpty())
                underlineCorrect();
            else
                underlineError();
        }

    }



    public void hideUnderline(){
//        mUnderlineStatus.setVisibility(GONE);
    }

}
