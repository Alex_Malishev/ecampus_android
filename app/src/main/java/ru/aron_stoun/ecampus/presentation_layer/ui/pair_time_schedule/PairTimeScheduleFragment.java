package ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseFragment;
import ru.aron_stoun.ecampus.presentation_layer.events.UpdateScheduleEvent;
import ru.aron_stoun.ecampus.utils.LogUtils;

@ActivityScope
public class PairTimeScheduleFragment extends BaseFragment implements PairTimeScheduleContract.PairTimeScheduleMvpView {

    private static final String TAG = PairTimeScheduleFragment.class.getSimpleName();
    @BindView(R.id.pair_time_schedule_recycler)
    RecyclerView pairTimeScheduleRecyclerView;

    @Inject PairTimeScheduleRecyclerAdapter pairTimeScheduleRecyclerAdapter;

    @Inject
    @InjectPresenter
    PairTimeSchedulePresenter pairTimeSchedulePresenter;

    @ProvidePresenter
    PairTimeSchedulePresenter pairTimeSchedulePresenter(){
        return pairTimeSchedulePresenter;
    }

    @Inject
    public PairTimeScheduleFragment() {
    }


    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pair_time_schedule_layout, container, false);
        ButterKnife.bind(this ,rootView);

        pairTimeScheduleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pairTimeScheduleRecyclerView.setAdapter(pairTimeScheduleRecyclerAdapter);


        pairTimeSchedulePresenter.getPairTimeSchedule();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public int setBackdropView() {
        return 0;
    }

    @Override
    public int setContentView() {
        return 0;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPairTimeSchedule(ArrayList<PairTime> pairTimes) {
        pairTimeScheduleRecyclerAdapter.clear();
        pairTimeScheduleRecyclerAdapter.addAll(pairTimes);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void update(UpdateScheduleEvent event){
        LogUtils.e(TAG, "update pair time schedule event");
        pairTimeScheduleRecyclerAdapter.highlightCurrent();
    }
}
