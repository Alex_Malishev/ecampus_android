package ru.aron_stoun.ecampus.presentation_layer.ui.subjects;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseFragment;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Speciality;

import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.academic_year.AcademicYearsRecyclerAdapter;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.academic_year.AcademicYearsView;
import ru.aron_stoun.ecampus.utils.LogUtils;

@ActivityScope
public class SubjectsFragment extends BaseFragment implements SubjectsContract.SubjectsView, AcademicYearsRecyclerAdapter.AcademicYearTermSelectionListener {


    private static final String TAG = SubjectsFragment.class.getSimpleName();
    private Unbinder mUnbinder;

    @BindView(R.id.academic_years_view)
    AcademicYearsView academicYearsView;

    @BindView(R.id.subjects_recycler_view)
    RecyclerView subjectsRecyclerView;

    @BindView(R.id.subjects_title)
    TextView subjectsTitle;

    @BindView(R.id.subjects_progress_bar)
    ProgressBar subjectProgressBar;

    @Inject
    SubjectsRecyclerAdapter subjectsRecyclerAdapter;

    @Inject
    @InjectPresenter
    SubjectsPresenter subjectsPresenter;

    @ProvidePresenter
    SubjectsPresenter subjectsPresenter(){
        return subjectsPresenter;
    }

    @Inject
    public SubjectsFragment() {
    }


    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, rootView);
        academicYearsView.setTermsSelectionListener(this);
        subjectsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        subjectsRecyclerView.setAdapter(subjectsRecyclerAdapter);
        subjectsRecyclerView.setNestedScrollingEnabled(false);
        subjectsPresenter.getSpecialities();


        return rootView;
    }

    @Override
    public int setBackdropView() {
        return R.layout.subject_backdrop;
    }

    @Override
    public int setContentView() {
        return R.layout.fragment_subjects;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();

    }


    @Override
    public void onSpecialities(ArrayList<Speciality> specialities) {
        academicYearsView.setSpecialities(specialities);
    }

    @Override
    public void onCourseList(ArrayList<Course> courses) {
        subjectsRecyclerAdapter.clear();
        subjectsRecyclerAdapter.addAll(courses);
    }

    @Override
    public void onTermSelected(int specialityId, int termId, String title) {
        LogUtils.e(TAG, "onTermSelected: " + specialityId + " and " + termId);
        subjectsTitle.setText(title);
        subjectsRecyclerAdapter.clear();
        subjectsPresenter.getCourses(specialityId, termId);

    }


    @Override
    public void onProgressStart() {
        super.onProgressStart();
        subjectProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgressStop() {
        super.onProgressStop();
        subjectProgressBar.setVisibility(View.GONE);

    }
}
