package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.domain_layer.models.Week;
import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseFragment;
import ru.aron_stoun.ecampus.presentation_layer.ui.custom.WeekSwitcher;
import ru.aron_stoun.ecampus.utils.ViewUtil;

@ActivityScope
public class MyScheduleFragment extends BaseFragment implements MyScheduleContract.MyScheduleMvpView, WeekSwitcher.WeekSelectionListener {


    private static final String TAG = MyScheduleFragment.class.getSimpleName();


    @BindView(R.id.week_switcher)
    WeekSwitcher weekSwitcher;
    @BindView(R.id.my_schedule_recycler_view)
    RecyclerView myScheduleRecyclerView;


    @Inject ScheduleRecyclerAdapter scheduleRecyclerAdapter;


    @Inject
    @InjectPresenter
    MySchedulePresenter mySchedulePresenter;

    @ProvidePresenter
    MySchedulePresenter mySchedulePresenter(){
        return mySchedulePresenter;
    }

    @Inject
    public MyScheduleFragment() {
    }


    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);

        myScheduleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        myScheduleRecyclerView.setNestedScrollingEnabled(false);
        myScheduleRecyclerView.setAdapter(scheduleRecyclerAdapter);
        weekSwitcher.setWeekSelectionListener(this);
        mySchedulePresenter.getMyScheduleWeeks();
        setBackdropTranslationY(ViewUtil.dpToPx(200));
        return rootView;
    }

    @Override
    public int setBackdropView() {
        return R.layout.schedule_backdrop;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public int setContentView() {
        return R.layout.my_schedule_fragment;
    }

    @Override
    public void onMyScheduleWeeks(ArrayList<Week> weeks, int currentIndex) {
        weekSwitcher.setWeeks(currentIndex, weeks);
    }

    @Override
    public void onSchedule(ArrayList<Weekday> weekdays) {
        scheduleRecyclerAdapter.clear();
        scheduleRecyclerAdapter.addAll(weekdays);
    }

    @Override
    public void onWeekClicked(Week week) {
        mySchedulePresenter.getSchedule(week.getDateBegin());
    }
}
