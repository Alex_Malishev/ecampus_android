package ru.aron_stoun.ecampus.presentation_layer.ui.auth;

import android.os.Bundle;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.BottomNavigationActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.login.LoginActivity;

public class SplashscreenActivity extends BaseActivity implements SplashscreenContract.SplashsscreenView {


    @Inject
    @InjectPresenter
    SplashscreenPresenter splashscreenPresenter;

    @ProvidePresenter
    SplashscreenPresenter splashscreenPresenter(){
        return splashscreenPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        splashscreenPresenter.tryToLogin();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSuccess() {
        startActivity(BottomNavigationActivity.getMainIntent(this));
    }

    @Override
    public void onDenied() {
        startActivity(LoginActivity.getLoginIntent(this));
    }
}
