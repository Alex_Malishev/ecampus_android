package ru.aron_stoun.ecampus.presentation_layer.base;

import android.content.IntentFilter;
import android.support.design.widget.Snackbar;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ru.aron_stoun.data_layer.utils.NetworkUtils;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.utils.ViewUtil;


public abstract class BaseConntectionFragment extends BaseFragment {

    Snackbar connectionSnackbar;
    View view;

    private NetworkStateReceiver networkStateReceiver;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        removeConnectionTitle();
    }

    @Override
    public void onResume() {
        super.onResume();

//        onConnectionStatusEvent(null);
    }


    public void registerConnectionReceiver(){
        if (getActivity() != null){
            networkStateReceiver = new NetworkStateReceiver();
            getActivity().registerReceiver(networkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    public void unregisterConnectionReceiver(){
        if (getActivity() != null){
            getActivity().unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }

    private void setConnectionTitle(int stringId){
        view = getSnackbarContainer();
        if (view != null && getContext() != null){
            view.setVisibility(View.VISIBLE);
            if (connectionSnackbar == null) {
                connectionSnackbar = ViewUtil.makeConnectionSnackbar(view, stringId, true);
                connectionSnackbar.show();
            }
        }
    }

    private void removeConnectionTitle(){
        if (connectionSnackbar != null && connectionSnackbar.isShownOrQueued()){
            connectionSnackbar.dismiss();
            connectionSnackbar = null;
            if (view != null)view.setVisibility(View.GONE);
        }
    }

    public abstract View getSnackbarContainer();


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectionStatusEvent(NetworkStateReceiver.ConnectionEvent connectionEvent){
        if (!NetworkUtils.isOnline()){
            setConnectionTitle(R.string.error_no_connection);
            return;
        }

        if (connectionEvent == null || !connectionEvent.isConnected()) {
            removeConnectionTitle();
            return;
        }else {
            setConnectionTitle(R.string.connection);
        }

    }
}
