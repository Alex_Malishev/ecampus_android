package ru.aron_stoun.ecampus.presentation_layer.ui.subjects;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseView;
import ru.aron_stoun.ecampus.presentation_layer.base.Presenter;

public interface SubjectsContract {

    interface SubjectsPresenter{
        void getSpecialities();
        void getCourses(int specialityId, int termId);
    }

    interface SubjectsView extends BaseView{
        void onSpecialities(ArrayList<Speciality> specialities);
        void onCourseList(ArrayList<Course> courses);
    }
}
