package ru.aron_stoun.ecampus.presentation_layer.base;

/**
 * Created by jarvi on 18.08.2017.
 */

public interface IRecyclerClickListener {

    void onItemClick(int position, Object o);
    boolean onItemLongClick(int position, Object o);
}
