package ru.aron_stoun.ecampus.presentation_layer.ui.auth.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.BottomNavigationActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.custom.AuthField;
import ru.aron_stoun.ecampus.presentation_layer.ui.custom.BaseEditText;

public class LoginActivity extends BaseActivity implements LoginContract.LoginMvpView {


    @BindView(R.id.username_field)
    AuthField usernameField;

    @BindView(R.id.password_field)
    AuthField passwordField;

    @BindView(R.id.login_button)
    Button loginButton;


    @Inject
    @InjectPresenter
    LoginPresenter loginPresenter;


    @ProvidePresenter
    LoginPresenter loginPresenter(){
        return loginPresenter;
    }

    public static Intent getLoginIntent(Context context){
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        BaseEditText.TextChangedListener textChangedListener = ()-> loginButton.setVisibility(!usernameField.isEmpty() && !passwordField.isEmpty()
                ? View.VISIBLE : View.INVISIBLE);

        usernameField.setTextChangedListener(textChangedListener);
        passwordField.setTextChangedListener(textChangedListener);





    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.login_button)
    @Optional
    public void login(){
        loginPresenter.login(usernameField.getText(), passwordField.getText());
    }


    @Override
    public void onLoginSuccess() {
        startActivity(BottomNavigationActivity.getMainIntent(this));
    }

    @Override
    public void onFieldsIsValid() {
        usernameField.underlineCorrect();
        passwordField.underlineCorrect();
    }

    @Override
    public void onUsernameEmpty() {
        usernameField.underlineError();
    }

    @Override
    public void onPasswordEmpty() {
        passwordField.underlineError();
    }

    @Override
    public void onError() {

    }
}
