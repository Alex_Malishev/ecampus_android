package ru.aron_stoun.ecampus.presentation_layer.base;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import ru.aron_stoun.ecampus.EcampusApp;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;
import ru.aron_stoun.ecampus.injection.component.DaggerCustomViewComponent;


public abstract class BaseCustomView extends RelativeLayout implements BaseView {


    public BaseCustomView(Context context) {
        super(context);
        injectDagger();
    }

    public BaseCustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        injectDagger();
    }

    public BaseCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        injectDagger();

    }

    private void injectDagger(){
        CustomViewComponent customViewComponent = EcampusApp.get().getCustomViewComponent();
        needInjection(customViewComponent);
    }

    public abstract void needInjection(CustomViewComponent customViewComponent);


    public void runOnUiThreed(Runnable runnable){
        try {
            new Handler(Looper.getMainLooper()).postDelayed(runnable, 100);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressStart() {

    }

    @Override
    public void onProgressStop() {

    }

    @Override
    public void onNetworkConnectionLost() {

    }

    @Override
    public void onTokenIsDead() {

    }
}
