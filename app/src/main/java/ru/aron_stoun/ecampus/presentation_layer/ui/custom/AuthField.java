package ru.aron_stoun.ecampus.presentation_layer.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import ru.aron_stoun.ecampus.R;

public class AuthField extends BaseEditText {


    public AuthField(@NonNull Context context) {
        super(context);
    }

    public AuthField(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AuthField(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    View initView(Context context) {
        View rootView = inflate(context, R.layout.auth_field, null);
        mTextField = rootView.findViewById(R.id.field);
        mClearButton = rootView.findViewById(R.id.cross_clear);
        inputLayout = rootView.findViewById(R.id.field_layout);
        return rootView;
    }

    @Override
    public void underlineCorrect() {
        inputLayout.setError(EMPTY);
        inputLayout.setErrorEnabled(false);
    }

    @Override
    public void underlineError() {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError("Пустое поле");
    }

}
