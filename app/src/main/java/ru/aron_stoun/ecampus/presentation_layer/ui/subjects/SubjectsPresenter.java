package ru.aron_stoun.ecampus.presentation_layer.ui.subjects;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectInteractor;
import ru.aron_stoun.domain_layer.usecase.subjects.SubjectsInteractor;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;
import ru.aron_stoun.domain_layer.models.AcademicYear;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.domain_layer.models.Term;
import ru.aron_stoun.ecampus.utils.LogUtils;

@ActivityScope
@InjectViewState
public class SubjectsPresenter extends BasePresenter<SubjectsContract.SubjectsView>
        implements SubjectsContract.SubjectsPresenter {


    private static final String TAG = SubjectsPresenter.class.getSimpleName();
    private ISubjectInteractor subjectsInteractor;

    @Inject
    public SubjectsPresenter(ISubjectInteractor subjectsInteractor) {
        this.subjectsInteractor = subjectsInteractor;
    }


    @SuppressLint("CheckResult")
    @Override
    public void getSpecialities() {
        addDisposable(subjectsInteractor.getSpecialities()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .map(getCourses())
                .subscribe(
                        this::onSpecialitiesSuccess,
                        throwable -> {
                            LogUtils.e(TAG, "error is " + throwable.getMessage());
                            LogUtils.printStackTrace(throwable);
                        },
                        () -> {},
                        disposable -> getViewState().onProgressStart()
                ));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getCourses(int specialityId, int termId) {
        addDisposable(subjectsInteractor.getCourses(specialityId, termId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        this::onCourseSuccess,
                        throwable -> {
                            LogUtils.e(TAG, "error is " + throwable.getMessage());
                            LogUtils.printStackTrace(throwable);
                        },
                        () -> getViewState().onProgressStop(),
                        disposable -> getViewState().onProgressStart()
                ));
    }


    /**
     * Method return {@link Function} for getting courses from current term
     * @return {@link Function}
     */
    private Function<List<Speciality>, List<Speciality>> getCourses() {
        return specialities -> {
            Speciality speciality = specialities.get(0);
            Term currentTerm = findCurrentTerm(speciality);
            if (currentTerm != null) getCourses(speciality.getId(), currentTerm.getId());
            return specialities;
        };
    }


    private void onCourseSuccess(List<Course> courses) {
            getViewState().onCourseList((ArrayList<Course>) courses);
    }


    private void onSpecialitiesSuccess(List<Speciality> specialities) {
            getViewState().onSpecialities((ArrayList<Speciality>) specialities);
    }


    /**
     * The method returns current {@link Term} in {@link Speciality}
     * @param speciality - {@link Speciality}
     * @return {@link Term}
     */
    private Term findCurrentTerm(Speciality speciality) {
        ArrayList<AcademicYear> academicYears = speciality.getAcademicYears();
        for (AcademicYear academicYear : academicYears) {
            ArrayList<Term> terms = academicYear.getTerms();
            for (Term term : terms) {
                if (term.isCurrent()) return term;
            }
        }

        return null;
    }
}
