package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.subject;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;
import ru.aron_stoun.domain_layer.models.LessonType;

public class LessonTypesRecyclerAdapter extends BaseRecyclerAdapter<LessonTypesRecyclerAdapter.LessonTypeHolder> {


    @Inject
    public LessonTypesRecyclerAdapter() {
        setUpdatable(false);
    }

    @NonNull
    @Override
    public LessonTypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_type_item, parent, false);
        return new LessonTypeHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonTypeHolder holder, int position) {
        LessonType lessonType = (LessonType) mCollection.get(position);
        holder.setLessonType(lessonType);
        super.onBindViewHolder(holder, position);
    }

    class LessonTypeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lesson_type_name)TextView lessonName;

        LessonTypeHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setLessonType(LessonType lessonType){
            lessonName.setText(lessonType.getName());
        }

    }

}
