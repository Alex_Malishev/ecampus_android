package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.domain_layer.models.ScheduleLesson;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;

public class LessonsRecyclerAdapter extends BaseRecyclerAdapter<LessonsRecyclerAdapter.LessonHolder> {

    @Inject
    public LessonsRecyclerAdapter() {
        setUpdatable(false);
    }

    @NonNull
    @Override
    public LessonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_list_item, parent, false);
        return new LessonHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonHolder holder, int position) {
        ScheduleLesson lesson = (ScheduleLesson) mCollection.get(position);
        holder.setLesson(lesson);
        super.onBindViewHolder(holder, position);
    }

    class LessonHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lesson_name)
        TextView name;

        @BindView(R.id.lesson_type_name)
        TextView type;

        @BindView(R.id.pair_number)
        TextView pairNumber;

        @BindView(R.id.pair_classroom)
        TextView pairClassroom;

        @BindView(R.id.teacher_name)
        TextView teacherName;

        LessonHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setLesson(ScheduleLesson lesson) {
            name.setText(lesson.getDiscipline());
            type.setText(lesson.getLessonType());
            String pairNumberText;
            if (lesson.getPairNumberStart() == lesson.getPairNumberEnd()) {
                pairNumberText = String.valueOf(lesson.getPairNumberStart());
            } else {
                pairNumberText = lesson.getPairNumberStart() + " - " + lesson.getPairNumberEnd();
            }
            pairNumberText += " пара";
            pairNumber.setText(pairNumberText);

            if (lesson.getClassroom() != null)pairClassroom.setText(lesson.getClassroom().getName());

            if (lesson.getTeacher() != null)teacherName.setText(lesson.getTeacher().getName());
        }
    }
}
