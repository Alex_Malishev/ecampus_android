package ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.domain_layer.models.PairTime;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;

public class PairTimeScheduleRecyclerAdapter extends BaseRecyclerAdapter<PairTimeScheduleRecyclerAdapter.PairTimeHolder> {

    private int activePosition = -1;

    @Inject
    public PairTimeScheduleRecyclerAdapter() {
        setUpdatable(false);
    }

    @Override
    public void addAll(Collection<?> objects) {
        super.addAll(objects);
        new Handler().postDelayed(this::highlightCurrent, 300);
    }

    private int findCurrent(){
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentMinute = rightNow.get(Calendar.MINUTE);
        for (int i = 0; i < mCollection.size(); i++) {
            PairTime pairTime = (PairTime)  mCollection.get(i);
            boolean isCurrent;
            if (pairTime.getHourStart() < currentHour && currentHour < pairTime.getHourEnd()) {
                isCurrent = true;
            }else if (pairTime.getHourStart() == currentHour){
                isCurrent = pairTime.getMinuteStart() <= currentMinute;
            }else if (pairTime.getHourEnd() == currentHour){
                isCurrent = pairTime.getMinuteEnd() >= currentMinute;
            }else {
                isCurrent = false;
            }

            if (isCurrent) return i;
        }
        return -1;
    }

    @NonNull
    @Override
    public PairTimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pair_time_list_item, parent, false);
        return new PairTimeHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull PairTimeHolder holder, int position) {
        PairTime pairTime = (PairTime) mCollection.get(position);
        holder.setPair(pairTime);
        if (activePosition == position){
            holder.highlight();
        }else {
            holder.unhighlight();
        }
        super.onBindViewHolder(holder, position);
    }

    @SuppressLint("CheckResult")
    public void highlightCurrent(){
        Observable.defer(()-> Observable.just(findCurrent()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(position -> {
                    if (activePosition != position){
                        int oldPosition = activePosition;
                        activePosition = position;
                        notifyItemChanged(oldPosition);
                        notifyItemChanged(position);
                    }
                });
    }



    class PairTimeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pair_name)
        TextView pairName;

        @BindView(R.id.pair_time)
        TextView pairTime;

        PairTimeHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setPair(PairTime pair){
            pairName.setText(pair.getName());
            pairTime.setText(pair.getStartTime().concat(" - ").concat(pair.getEndTime()));
        }

        void highlight() {
            itemView.setBackgroundResource(R.drawable.empty_white_rectangle);
        }

        void unhighlight() {
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.transparent));
        }
    }
}
