package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import java.util.ArrayList;

import ru.aron_stoun.domain_layer.models.Week;
import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseView;
import ru.aron_stoun.ecampus.presentation_layer.base.Presenter;

public interface MyScheduleContract {

    interface MySchedulePresenter{
        void getMyScheduleWeeks();
        void getSchedule(String date);
    }

    interface MyScheduleMvpView extends BaseView{
        void onMyScheduleWeeks(ArrayList<Week> weeks, int currentIndex);
        void onSchedule(ArrayList<Weekday> weekdays);
    }
}
