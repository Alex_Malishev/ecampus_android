package ru.aron_stoun.ecampus.presentation_layer.ui.auth.login;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthInteractor;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.base.BasePresenter;
import ru.aron_stoun.ecampus.utils.LogUtils;

@ActivityScope
@InjectViewState
public class LoginPresenter extends BasePresenter<LoginContract.LoginMvpView> implements LoginContract.LoginPresenter {


    private static final String TAG = LoginPresenter.class.getSimpleName();
    private IAuthInteractor authInteractor;

    @Inject
    public LoginPresenter(IAuthInteractor authInteractor) {
        this.authInteractor = authInteractor;
    }

    @SuppressLint("CheckResult")
    @Override
    public void login(String username, String password) {
        if (!validateField(username, password)) return;
        addDisposable(authInteractor.login(username, password ,false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(this::checkLoginIsSuccess, this::loginError));
    }

    private void checkLoginIsSuccess(Boolean result){
        if (result ){
            getViewState().onLoginSuccess();
        }
    }

    private void loginError(Throwable throwable){
            LogUtils.e(TAG, "login error ");
            throwable.printStackTrace();
            getViewState().onError();
    }

    private boolean validateField(String username, String password){
        boolean result = true;
        if (username.isEmpty()){
            result = false;
            getViewState().onUsernameEmpty();
        }

        if (password.isEmpty()){
            result = false;
            getViewState().onPasswordEmpty();
        }

        if (result){
            getViewState().onFieldsIsValid();
        }
        return result;
    }
}
