package ru.aron_stoun.ecampus.presentation_layer.base;

import android.support.v4.app.Fragment;

import dagger.android.support.DaggerFragment;


/**
 * Created by jarvi on 25.08.2017.
 */

public class BaseChildFragment extends DaggerFragment implements BaseView{


    public void setTitle(String title){
        if (getActivity() != null) getActivity().setTitle(title);
    }

    public void show(Fragment baseFragment, String name){
//        ((BaseActivity) getActivity()).show(baseFragment, name);
    }

    @Override
    public void onProgressStart() {

    }

    @Override
    public void onProgressStop() {

    }

    @Override
    public void onNetworkConnectionLost() {

    }

    @Override
    public void onTokenIsDead() {

    }


}
