package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.subject;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseCustomView;
import ru.aron_stoun.domain_layer.models.Course;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;

public class SubjectView extends BaseCustomView {

    @BindView(R.id.lesson_types_recycler_view)
    RecyclerView lessonTypeRecyclerView;

    @BindView(R.id.subject_name)
    TextView subjectName;

    @BindView(R.id.subject_sum_rating)
    TextView subjectSumRating;

    private Course course;

    @Inject LessonTypesRecyclerAdapter lessonTypesRecyclerAdapter;


    public SubjectView(Context context) {
        super(context);
        init();
    }

    public SubjectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SubjectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.subject_item_layout, this);
        ButterKnife.bind(this, rootView);
        lessonTypeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        lessonTypeRecyclerView.setAdapter(lessonTypesRecyclerAdapter);
    }

    public void setCourse(Course course) {
        this.course = course;
        subjectName.setText(course.getName());
        String sumRatingText = String.valueOf(course.getCurrentRating()) + " " +
                getContext().getString(R.string.of) + " " + course.getMaxRating();
        subjectSumRating.setText(sumRatingText);
        lessonTypesRecyclerAdapter.clear();
        lessonTypesRecyclerAdapter.addAll(course.getLessonTypes());
    }

    @Override
    public void needInjection(CustomViewComponent customViewComponent) {
        customViewComponent.inject(this);
    }
}
