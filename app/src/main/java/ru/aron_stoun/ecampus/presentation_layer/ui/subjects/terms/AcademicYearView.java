package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.terms;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseCustomView;
import ru.aron_stoun.ecampus.presentation_layer.base.IRecyclerClickListener;
import ru.aron_stoun.domain_layer.models.AcademicYear;
import ru.aron_stoun.domain_layer.models.Term;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;

public class AcademicYearView extends BaseCustomView implements IRecyclerClickListener {


    @BindView(R.id.course_name)
    TextView mCourseName;

    @BindView(R.id.terms_recycler)
    RecyclerView mTermsRecyclerView;

    @Inject TermsRecyclerAdapter termsRecyclerAdapter;
    private TermSelectionListener termSelectionListener;
    private AcademicYear academicYear;

    public interface TermSelectionListener{
        void onTermSelected(int specialityId, int termId, String title);
    }

    public void setTermSelectionListener(TermSelectionListener termSelectionListener) {
        this.termSelectionListener = termSelectionListener;
    }

    public AcademicYearView(Context context) {
        super(context);
        init();
    }

    public AcademicYearView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AcademicYearView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        View rootView = inflate(getContext(), R.layout.academic_year_view, this);
        ButterKnife.bind(rootView);

        mTermsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mTermsRecyclerView.setAdapter(termsRecyclerAdapter);
        termsRecyclerAdapter.setmRecyclerClickListener(this);
    }

    public void unhighlightTerms(){
        termsRecyclerAdapter.removeActiveUserPosition();
    }


    public void setInfo(AcademicYear academicYear){
        this.academicYear = academicYear;
        String courseName = academicYear.getName().concat(" ").concat(academicYear.getCourseTypeName());
        mCourseName.setText(courseName);
        termsRecyclerAdapter.clear();
        termsRecyclerAdapter.addAll(academicYear.getTerms());

    }

    @Override
    public void needInjection(CustomViewComponent customViewComponent) {
        customViewComponent.inject(this);
    }

    @Override
    public void onItemClick(int position, Object o) {
        if (termSelectionListener != null){
            Term term = (Term) o;
            String courseName = academicYear.getName().concat(" ").concat(academicYear.getCourseTypeName());
            String title = courseName + " (" + term.getName() + " " + term.getTermTypeName() + ")";
            termSelectionListener.onTermSelected(academicYear.getParentId(), term.getId(), title);
        }
    }

    @Override
    public boolean onItemLongClick(int position, Object o) {
        return false;
    }
}
