package ru.aron_stoun.ecampus.presentation_layer.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by jarvi on 18.08.2017.
 */

public abstract class BaseRecyclerAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private IRecyclerClickListener mRecyclerClickListener;
    private RecyclerEndListener mRecyclerEndListener;
    public ArrayList<Object> mCollection = new ArrayList<>();

    public static final int LIMIT = 25;
    public static final int LIMIT_SEARCH = 50;
    public static final int FIRST_PAGE = 0;

    private int mCurrentPage = 1;
    private boolean isNeedUpdate = true;

    public void addAll(Collection<?> objects){
        mCollection.addAll(objects);
//        notifyItemRangeInserted(mCollection.size(), objects.size());
        notifyDataSetChanged();
    }

    public void addAllWithoutNotify(Collection<?> objects){
        mCollection.addAll(objects);
    }

    public void clear(){
        mCurrentPage = 1;
        mCollection.clear();
        notifyDataSetChanged();
    }

    public void setCurrentPage(int mCurrentPage) {
        this.mCurrentPage = mCurrentPage;
    }

    public void clearWithoutNotify(){
        mCurrentPage = 1;
        mCollection.clear();
    }

    public void setUpdatable(boolean isUpdatable){
        isNeedUpdate = isUpdatable;
    }

    public void setmRecyclerClickListener(IRecyclerClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setmRecyclerEndListener(RecyclerEndListener mRecyclerEndListener) {
        this.mRecyclerEndListener = mRecyclerEndListener;
    }

    @Override
    public void onBindViewHolder(@NonNull final T holder, int position) {
        if (mRecyclerClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick(holder);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return onItemLongClick(holder);
                }
            });
        }

        if (mCollection.size() - position <= 3 && isNeedUpdate && mCollection.size() >= LIMIT){
            if (mRecyclerEndListener != null) {
                mCurrentPage++;
                mRecyclerEndListener.onRecyclerEnd(mCurrentPage);

            }
        }

    }

    public void onItemClick(@NonNull T holder){
        mRecyclerClickListener.onItemClick(holder.getAdapterPosition(), mCollection.get(holder.getAdapterPosition()));
    }

    public boolean onItemLongClick(@NonNull T holder){
        return mRecyclerClickListener.onItemLongClick(holder.getAdapterPosition(), mCollection.get(holder.getAdapterPosition()));

    }

    @Override
    public int getItemCount() {
        return mCollection.size();
    }

    public interface RecyclerEndListener {
        void onRecyclerEnd(int nextPage);
    }

}
