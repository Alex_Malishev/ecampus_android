package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.academic_year;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.domain_layer.models.AcademicYear;
import ru.aron_stoun.domain_layer.models.Speciality;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseCustomView;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;

public class AcademicYearsView extends BaseCustomView {


    @BindView(R.id.academic_years_recycler_view)
    RecyclerView academicYearsRecyclerView;

    @BindView(R.id.specialities)
    AppCompatSpinner specialitiesSpinner;


    private ArrayList<Speciality> mSpecialities;

    @Inject
    AcademicYearsRecyclerAdapter academicYearsRecyclerAdapter;

    public AcademicYearsView(Context context) {
        super(context);
        init();
    }

    public AcademicYearsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AcademicYearsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.academic_years_layout, this);
        ButterKnife.bind(this, rootView);
        invalidate();
        mSpecialities = new ArrayList<>();
        academicYearsRecyclerView.setAdapter(academicYearsRecyclerAdapter);
        academicYearsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL) {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view);
                // hide the divider for the last child
                if (position == Objects.requireNonNull(parent.getAdapter()).getItemCount() - 1) {
                    outRect.setEmpty();
                } else {
                    super.getItemOffsets(outRect, view, parent, state);
                }
            }
        };
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.divider);
        if (drawable != null) itemDecorator.setDrawable(drawable);
        academicYearsRecyclerView.addItemDecoration(itemDecorator);

    }

    public void setTermsSelectionListener(AcademicYearsRecyclerAdapter.AcademicYearTermSelectionListener academicYearTermSelectionListener) {
        academicYearsRecyclerAdapter.setAcademicYearTermSelectionListener(academicYearTermSelectionListener);
    }


    public void setSpecialities(ArrayList<Speciality> specialities) {
        ArrayList<String> specialitiesNames = new ArrayList<>();
        mSpecialities.clear();
        mSpecialities.addAll(specialities);
        for (Speciality speciality : specialities) {
            specialitiesNames.add(speciality.getName());
        }
        setSpinner(specialitiesNames);

    }

    private void setSpeciality(Speciality speciality) {
        academicYearsRecyclerAdapter.clear();
        ArrayList<AcademicYear> academicYears = speciality.getAcademicYears();
        academicYearsRecyclerAdapter.addAll(academicYears);
        academicYearsRecyclerView.postDelayed(()->scrollToCurrent(academicYears), 400);

    }

    private void scrollToCurrent(ArrayList<AcademicYear> academicYears) {
        for (int i = 0; i < academicYears.size(); i++) {
            AcademicYear academicYear = academicYears.get(i);
            if (academicYear.isCurrent()) {
                academicYearsRecyclerView.smoothScrollToPosition(i);
                break;
            }
        }
    }

    private void setSpinner(ArrayList<String> specialitiesNames) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, specialitiesNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        specialitiesSpinner.setAdapter(adapter);

        // выделяем элемент
        specialitiesSpinner.setSelection(0);
        // устанавливаем обработчик нажатия
        specialitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                try {
                    setSpeciality(mSpecialities.get(position));
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    @Override
    public void needInjection(CustomViewComponent customViewComponent) {
        customViewComponent.inject(this);
    }

}
