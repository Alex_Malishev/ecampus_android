package ru.aron_stoun.ecampus.presentation_layer.ui.schedule;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.domain_layer.models.ScheduleLesson;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.domain_layer.models.Weekday;
import ru.aron_stoun.ecampus.injection.component.CustomViewComponent;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseCustomView;
import ru.aron_stoun.ecampus.utils.DateUtils;

public class WeekdayView extends BaseCustomView {


    private Weekday weekday;

    @BindView(R.id.group_name)
    TextView groupName;

    @BindView(R.id.weekday_name)
    TextView weekdayName;

    @BindView(R.id.weekday_lessons_recycler)
    RecyclerView weekdaysLessonsRecyclerView;

    @Inject
    LessonsRecyclerAdapter lessonsRecyclerAdapter;

    public WeekdayView(Context context) {
        super(context);
        init();
    }

    public WeekdayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WeekdayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void needInjection(CustomViewComponent customViewComponent) {
        customViewComponent.inject(this);
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.weekday_view, this);
        ButterKnife.bind(this, rootView);

        weekdaysLessonsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

        weekdaysLessonsRecyclerView.setAdapter(lessonsRecyclerAdapter);


    }

    public void setWeekday(Weekday weekday) {
        this.weekday = weekday;
        String weekdayNameText = weekday.getName() + ", " + DateUtils.formatDateWithWord(weekday.getDate());
        weekdayName.setText(weekdayNameText);
        if (weekday.getLessons() != null && !weekday.getLessons().isEmpty()) {
            ScheduleLesson scheduleLesson = weekday.getLessons().get(0);
            if (scheduleLesson.getGroups() != null && !scheduleLesson.getGroups().isEmpty()) {
                groupName.setText(scheduleLesson.getGroups().get(0).getName());
            }
        }

        lessonsRecyclerAdapter.clear();
        lessonsRecyclerAdapter.addAll(weekday.getLessons());
    }
}
