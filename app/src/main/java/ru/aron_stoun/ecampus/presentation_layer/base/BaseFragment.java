package ru.aron_stoun.ecampus.presentation_layer.base;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;
import dagger.android.support.HasSupportFragmentInjector;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.ui.BottomNavigationActivity;


/**
 * Created by jarvi on 06.06.2017.
 */

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseView, HasSupportFragmentInjector {

    private static final String TAG = BaseFragment.class.getSimpleName();
    private Toolbar mToolbar;
    private TextView mTextMessage;
    private AppBarLayout appBarLayout;
    private LinearLayout backdropContainer;

    private String currentTitle;
    private NavigationIconClickListener navigationIconClickListener;

    @Inject
    DispatchingAndroidInjector<Fragment> childFragmentInjector;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return childFragmentInjector;
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        appBarLayout = view.findViewById(R.id.app_bar);
        mTextMessage = (TextView) view.findViewById(R.id.text_message);
        backdropContainer = view.findViewById(R.id.backdrop_container);

        if (getActivity() != null) {
            ((BottomNavigationActivity)getActivity()).setSupportActionBar(mToolbar);
            ((BottomNavigationActivity)getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        }
        backdropContainer.removeAllViews();
        if (setBackdropView() != 0){
            inflater.inflate(setBackdropView(), backdropContainer, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getContext() != null) {
                view.findViewById(R.id.subjects_container).setBackground(getContext().getDrawable(R.drawable.shr_background_shape));
            }
            setUpToolbar(view);
        }
        RelativeLayout containerBase = (RelativeLayout) view.findViewById(R.id.base_content);
        containerBase.addView(inflater.inflate(setContentView(), containerBase, false));
        return view;
    }

    public abstract int setBackdropView();

    public void removeToolbarElevation(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            appBarLayout.setOutlineProvider(null);
            appBarLayout.setElevation(0);
            mToolbar.setElevation(0);
        }
    }

    private void setUpToolbar(View view) {
//        Toolbar toolbar = view.findViewById(R.id.toolbar);
//        AppCompatActivity activity = (AppCompatActivity) getActivity();
//        if (activity != null) {
//            activity.setSupportActionBar(toolbar);
//        }
        if (getContext() == null) return;
//        mToolbar = toolbar;
        navigationIconClickListener = new NavigationIconClickListener(
                getContext(),
                view.findViewById(R.id.subjects_container),
                new AccelerateDecelerateInterpolator(),
                getContext().getResources().getDrawable(R.drawable.shr_menu), // Menu open icon
                getContext().getResources().getDrawable(R.drawable.shr_close_menu));
        mToolbar.setNavigationOnClickListener(navigationIconClickListener); // Menu close icon

    }

    public void setBackdropTranslationY(int y){
        navigationIconClickListener.setTranslateY(y);
    }

    public void setCustomToolbar(Toolbar toolbar){
        mToolbar = toolbar;
        if (getActivity() != null) {
            ((BottomNavigationActivity)getActivity()).setSupportActionBar(mToolbar);
        }
    }

    public abstract int setContentView();


    public void setTitle(String title) {
        currentTitle = title;
        mToolbar.setTitle(title);
    }

    public void show(Fragment baseFragment, String name) {

    }


    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }




    @Override
    public void onProgressStart() {

    }

    @Override
    public void onProgressStop() {

    }


    @Override
    public void onNetworkConnectionLost() {

    }

    public void showMessage(String message) {
        mTextMessage.setVisibility(View.VISIBLE);
        mTextMessage.setText(message);

    }

    public void hideMessage() {
        mTextMessage.setVisibility(View.GONE);
    }

    @Override
    public void onTokenIsDead() {

    }

    public void runOnUiThread(Runnable runnable) {
        if (getActivity() != null){
            getActivity().runOnUiThread(runnable);
        }
    }


}
