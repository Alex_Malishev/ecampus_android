package ru.aron_stoun.ecampus.presentation_layer.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jarvi on 31.08.2017.
 */

public abstract class BaseUpdatableFragment extends BaseFragment
        implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;


    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(setSwipeRefreshLayout());
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return rootView;
    }

    public void setRefreshing(boolean isRefreshing){
        mSwipeRefreshLayout.setRefreshing(isRefreshing);
    }

    public abstract int setSwipeRefreshLayout();

    @Override
    public void onProgressStart() {
        setRefreshing(true);
    }

    @Override
    public void onProgressStop() {
        setRefreshing(false);
    }
}
