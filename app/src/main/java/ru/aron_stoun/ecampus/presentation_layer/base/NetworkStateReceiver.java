package ru.aron_stoun.ecampus.presentation_layer.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import ru.aron_stoun.ecampus.utils.LogUtils;



/**
 * Created by aron_stoun on 29.12.17.
 */

public class NetworkStateReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkStateReceiver.class.getSimpleName();

    @Inject
    public NetworkStateReceiver() {

    }


    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent == null || intent.getExtras() == null)
            return;
        LogUtils.e(TAG, "on Receive network update");
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = null;
        if (manager != null) {
            ni = manager.getActiveNetworkInfo();
        }
        Boolean connected;

        if(ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
            connected = false;
        }else {
            connected = false;
        }

        notifyStateToAll(connected);
    }

    private void notifyStateToAll(boolean isConnected) {
        EventBus.getDefault().post(new ConnectionEvent(isConnected));
    }

    public class ConnectionEvent {
        boolean isConnected;

        ConnectionEvent(boolean isConnected) {
            this.isConnected = isConnected;
        }

        public boolean isConnected() {
            return isConnected;
        }
    }


}
