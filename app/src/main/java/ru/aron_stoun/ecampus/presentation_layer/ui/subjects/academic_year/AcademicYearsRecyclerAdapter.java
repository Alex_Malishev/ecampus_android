package ru.aron_stoun.ecampus.presentation_layer.ui.subjects.academic_year;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseRecyclerAdapter;
import ru.aron_stoun.domain_layer.models.AcademicYear;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.terms.AcademicYearView;

public class AcademicYearsRecyclerAdapter extends BaseRecyclerAdapter<AcademicYearsRecyclerAdapter.AcademicYearHolder> {

    public interface AcademicYearTermSelectionListener{
        void onTermSelected(int specialityId, int termId, String title);
    }



    private AcademicYearTermSelectionListener academicYearTermSelectionListener;
    private int activeUserPosition = -1;


    @Inject
    public AcademicYearsRecyclerAdapter() {
    }

    public void setAcademicYearTermSelectionListener(AcademicYearTermSelectionListener academicYearTermSelectionListener) {
        this.academicYearTermSelectionListener = academicYearTermSelectionListener;
    }


    @NonNull
    @Override
    public AcademicYearHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AcademicYearView rootView = new AcademicYearView(parent.getContext());
        return new AcademicYearHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull AcademicYearHolder holder, int position) {
        AcademicYear academicYear = (AcademicYear) mCollection.get(position);
        holder.setAcademicYear(academicYear);
        if (activeUserPosition != position){
            holder.unhighlightTerm();
        }
        super.onBindViewHolder(holder, position);
    }

    class AcademicYearHolder extends RecyclerView.ViewHolder implements AcademicYearView.TermSelectionListener{

        AcademicYearHolder(@NonNull AcademicYearView itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setTermSelectionListener(this);

        }

        void setAcademicYear(AcademicYear academicYear){
            ((AcademicYearView) itemView).setInfo(academicYear);
        }

        @Override
        public void onTermSelected(int specialityId, int termId, String title) {
            if (getAdapterPosition() != -1){
                activeUserPosition = getAdapterPosition();
                notifyDataSetChanged();
            }
            if (academicYearTermSelectionListener != null){
                academicYearTermSelectionListener.onTermSelected(specialityId, termId, title);
            }
        }


        void unhighlightTerm(){
            ((AcademicYearView) itemView).unhighlightTerms();
        }
    }
}
