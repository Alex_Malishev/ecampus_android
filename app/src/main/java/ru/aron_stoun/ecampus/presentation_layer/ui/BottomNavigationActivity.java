package ru.aron_stoun.ecampus.presentation_layer.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aron_stoun.ecampus.R;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseActivity;
import ru.aron_stoun.ecampus.presentation_layer.base.BaseFragment;
import ru.aron_stoun.ecampus.presentation_layer.events.UpdateScheduleEvent;
import ru.aron_stoun.ecampus.presentation_layer.ui.schedule.MyScheduleFragment;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.SubjectsFragment;
import ru.aron_stoun.ecampus.utils.ViewUtil;

public class BottomNavigationActivity extends BaseActivity implements SlidingPaneLayout.PanelSlideListener {


    public static final String TAB_1 = "Subjects";
    public static final String TAB_2 = "Schedule";
    public static final String TAB_3 = "Search";
    public static final String TAB_4 = "Profile";
    private static final String CURRENT_POSITION = "current_position";

    @BindView(R.id.message) TextView mTextMessage;
    @BindView(R.id.navigation) BottomNavigationView navigationView;
    @BindView(R.id.sliding_pane_layout)
    SlidingPaneLayout slidingPaneLayout;

    @Inject MyScheduleFragment myScheduleFragment;
    @Inject SubjectsFragment subjectsFragment;

    private List<BaseFragment> fragments = new ArrayList<>(4);
    private static int sCurrentPosition = 0;


    public static Intent getMainIntent(Context context){
        return new Intent(context, BottomNavigationActivity.class);
    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_my_subjects:
                        switchFragment(0);
                        return true;
                    case R.id.navigation_my_schedule:
                        switchFragment(1);
                        return true;
                    case R.id.navigation_search:
                        switchFragment(0);
                        return true;
                }
                return false;
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        ButterKnife.bind(this);
        buildFragmentsList();
        ViewUtil.removeShiftMode(navigationView);
        slidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
        slidingPaneLayout.setPanelSlideListener(this);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigationView.setSelectedItemId(getMenuItemId());


    }


    private void buildFragmentsList() {
        fragments.add(subjectsFragment);
        fragments.add(myScheduleFragment);
    }

    private void switchFragment(int pos) {
        String tag = TAB_1;
        int itemId = R.id.navigation_my_subjects;
        switch (pos){
            case 0:
                itemId = R.id.navigation_my_subjects;
                tag = TAB_1;
                break;
            case 1:
                itemId = R.id.navigation_my_schedule;
                tag = TAB_2;
                break;
            case 2:
                itemId = R.id.navigation_search;
                tag = TAB_3;
                break;
        }
        sCurrentPosition = pos;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_fragmentholder, fragments.get(pos), tag)
                .commit();
    }

    private int getMenuItemId(){
        int itemId = R.id.navigation_my_subjects;
        switch (sCurrentPosition){
            case 0:
                itemId = R.id.navigation_my_subjects;
                break;
            case 1:
                itemId = R.id.navigation_my_schedule;
                break;
            case 2:
                itemId = R.id.navigation_search;
                break;
        }
        return itemId;
    }


    @Override
    public void onPanelSlide(@NonNull View panel, float slideOffset) {

    }

    @Override
    public void onPanelOpened(@NonNull View panel) {
        EventBus.getDefault().post(new UpdateScheduleEvent());
    }

    @Override
    public void onPanelClosed(@NonNull View panel) {

    }
}
