package ru.aron_stoun.ecampus.injection.module;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.injection.module.activity.BottomNavigationModule;
import ru.aron_stoun.ecampus.injection.module.activity.LoginModule;
import ru.aron_stoun.ecampus.injection.module.activity.SplashscreenModule;
import ru.aron_stoun.ecampus.presentation_layer.ui.BottomNavigationActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.SplashscreenActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.login.LoginActivity;


@Module(includes = AndroidInjectionModule.class)
abstract public class ActivityModule {


    @ActivityScope
    @ContributesAndroidInjector(modules = SplashscreenModule.class)
    abstract SplashscreenActivity provideSplashscreenActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginModule.class)
    abstract LoginActivity provideLoginActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = BottomNavigationModule.class)
    abstract BottomNavigationActivity provideBottomNavigationActivity();



}
