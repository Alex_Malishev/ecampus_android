package ru.aron_stoun.ecampus.injection.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import ru.aron_stoun.ecampus.injection.ApplicationContext;
import ru.aron_stoun.ecampus.presentation_layer.ui.BottomNavigationActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.SplashscreenActivity;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.login.LoginActivity;

/**
 * Provide application-level dependencies.
 */
@Module
abstract public class ApplicationModule {

    @Binds
    abstract Context bindContext(Application application);





}
