package ru.aron_stoun.ecampus.injection.module.activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.injection.FragmentScope;
import ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule.PairTimeScheduleContract;
import ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule.PairTimeScheduleFragment;
import ru.aron_stoun.ecampus.presentation_layer.ui.pair_time_schedule.PairTimeSchedulePresenter;
import ru.aron_stoun.ecampus.presentation_layer.ui.schedule.MyScheduleContract;
import ru.aron_stoun.ecampus.presentation_layer.ui.schedule.MyScheduleFragment;
import ru.aron_stoun.ecampus.presentation_layer.ui.schedule.MySchedulePresenter;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.SubjectsContract;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.SubjectsFragment;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.SubjectsPresenter;

@Module
abstract public class BottomNavigationModule {

    /**
     * Pair Time Schedule
     * */
    @FragmentScope
    @ContributesAndroidInjector
    abstract PairTimeScheduleFragment providePairTimeScheduleFragment();

    /**
     * My Schedule
     * */
    @FragmentScope
    @ContributesAndroidInjector
    abstract MyScheduleFragment provideMyScheduleFragment();



    /**
     * My Subjects
     * */
    @FragmentScope
    @ContributesAndroidInjector
    abstract SubjectsFragment provideSubjectsFragment();




}
