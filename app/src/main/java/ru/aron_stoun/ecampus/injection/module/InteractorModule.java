package ru.aron_stoun.ecampus.injection.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.aron_stoun.data_layer.injection.DataSourceModule;
import ru.aron_stoun.domain_layer.usecase.auth.AuthInteractor;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthInteractor;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthRepository;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleInteractor;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleRepository;
import ru.aron_stoun.domain_layer.usecase.my_schedule.ScheduleInteractor;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectInteractor;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectsRepository;
import ru.aron_stoun.domain_layer.usecase.subjects.SubjectsInteractor;


@Module
public class InteractorModule {

    public InteractorModule() {
    }

    @Provides
    ISubjectInteractor provideSubjectsInteractor(ISubjectsRepository subjectsRepository){
        return new SubjectsInteractor(subjectsRepository);
    }

    @Provides
    IScheduleInteractor provideShcScheduleInteractor(IScheduleRepository iScheduleRepository){
        return new ScheduleInteractor(iScheduleRepository);
    }

    @Provides
    IAuthInteractor provideAuthInteractor(IAuthRepository iAuthRepository){
        return new AuthInteractor(iAuthRepository);
    }
}
