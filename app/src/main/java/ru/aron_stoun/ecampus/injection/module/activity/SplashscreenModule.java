package ru.aron_stoun.ecampus.injection.module.activity;

import dagger.Binds;
import dagger.Module;
import ru.aron_stoun.ecampus.injection.ActivityScope;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.SplashscreenContract;
import ru.aron_stoun.ecampus.presentation_layer.ui.auth.SplashscreenPresenter;

@Module
abstract public class SplashscreenModule {

}
