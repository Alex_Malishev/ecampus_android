package ru.aron_stoun.ecampus.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.aron_stoun.ecampus.injection.PerApp;
import ru.aron_stoun.ecampus.injection.PerView;
import ru.aron_stoun.ecampus.injection.module.CustomViewModule;
import ru.aron_stoun.ecampus.presentation_layer.ui.schedule.WeekdayView;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.terms.AcademicYearView;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.academic_year.AcademicYearsView;
import ru.aron_stoun.ecampus.presentation_layer.ui.subjects.subject.SubjectView;


@Component(modules = CustomViewModule.class)
public interface CustomViewComponent {
    void inject(AcademicYearsView academicYearView);

    void inject(AcademicYearView termView);

    void inject(SubjectView subjectView);

    void inject(WeekdayView weekdayView);
}
