package ru.aron_stoun.ecampus.injection.module;


import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class CustomViewModule {

    private Context context;

    public CustomViewModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext(){
        return context;
    }


}
