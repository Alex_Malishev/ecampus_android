package ru.aron_stoun.ecampus.injection.component;

import android.app.Application;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import ru.aron_stoun.data_layer.injection.DataLayerComponent;
import ru.aron_stoun.domain_layer.usecase.auth.IAuthInteractor;
import ru.aron_stoun.domain_layer.usecase.my_schedule.IScheduleInteractor;
import ru.aron_stoun.domain_layer.usecase.subjects.ISubjectInteractor;
import ru.aron_stoun.ecampus.EcampusApp;
import ru.aron_stoun.ecampus.injection.PerApp;
import ru.aron_stoun.ecampus.injection.module.ActivityModule;
import ru.aron_stoun.ecampus.injection.module.ApplicationModule;
import ru.aron_stoun.ecampus.injection.module.InteractorModule;


@PerApp
@Component( dependencies = {DataLayerComponent.class, CustomViewComponent.class}, modules = {
        ApplicationModule.class,
        ActivityModule.class,
        InteractorModule.class,
        AndroidSupportInjectionModule.class})
public interface ApplicationComponent extends AndroidInjector<EcampusApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        ApplicationComponent.Builder application(Application application);
        ApplicationComponent.Builder dataLayer(DataLayerComponent dataLayerComponent);
        ApplicationComponent.Builder customView(CustomViewComponent customViewComponent);

        ApplicationComponent build();
    }




}
